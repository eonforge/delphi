package com.eonforge.delphi.search;

import java.util.HashMap;
import java.util.Map;
import com.eonforge.delphi.db.Datum;

public class Doc extends Datum {

    public String collection;
    public Doc(String collection) { this.collection = collection; }

    @Override
    public String table() { return collection; }

    @Override
    public Datum newInstance(Map<String, Object>... data) {
        Doc inst = new Doc(collection);
	for (Map<String, Object> dataMap : data)
	    for (String key : dataMap.keySet()) 
	        inst.put(key, dataMap.get(key));
	return inst;
    }

    @Override
    public Map<String, Object> colMap() {
        return (Map<String, Object>) this;
    }

}
