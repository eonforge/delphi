package com.eonforge.delphi.db.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.eonforge.delphi.db.Datum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Node extends Datum {

    private static final Logger log = LoggerFactory.getLogger(Node.class);
    public String text = "", begin = "", end = "", name = "", score = "";
    public String id = null, templateid = "", pkg="", inherit="", modifiers="";
    public String generic = null, classtype = null, classdef = null;
    public List<String> servers = new ArrayList(), annotations = null;
    public long executionTime = -1l, level = 0;
    public int location = 0, length = -1;
    public List<Node> nodes = new ArrayList(), innerClasses = null;
    public Map<String, Map<Node, String>> matchNodeMap = new HashMap();
    public Node parent = null;
    public Boolean compiles = null, runs = null;
    public boolean isComment = false, isString = false, hasMain = false,
        hasCurly = false, hasBraces = false, isClass = false, isMethod = false;
    public String filename = null, retval = null, flattened = null;
    public List<String> methods = null, ctors = null, fields = null, 
                        classdefs = null, classmembers = null;
    public Set<String> methodTypes = null;
    public List<String> qualifiers = null;
    public List<String> texts = new ArrayList();
    public List<List<Node>> textNodes = new ArrayList();
    public Map<String, List<String>> pathMap = new HashMap();
    public List<List<Node>> members = null, aggregates = null;
    public String appInstance = null, owner = null;

    public static final String SCOPE_REGEX = 
	    "((public|protected|private)[\\s\n]+)";

    public static final String ANNOTATION_REGEX = 
        "(" + SCOPE_REGEX + "*\\@[A-Za-z0-9\\.]+(\\([^\\)]*\\))?[\\s\n]*)*";

    public static final String CLASS_REGEX = ANNOTATION_REGEX +
	SCOPE_REGEX + "*(" +
	"(static|final|abstract|synchronized)[\\s\n]+)*" +
        "(class|enum|interface)[\\s\n]+" +
	"([^\\s\n]+(<[^>]*>)?[\\s\n]*)" +
	"((extends|implements)[\\s\n]+" +
	"([A-Z][A-Za-z0-9_,\\.]*(<.+>)?[\\s\n,]*)+)*\\{\\s*";

    public static final String ENUM_REGEX =
        ANNOTATION_REGEX.replace("(class|enum|interface)", "[Ee]num") +
        "[\\s\n]*[A-Z0-9_]+(\\([^\\)]*\\))?[\\s\n]*[,;]";

    public static final String MODIFIER_REGEX = ANNOTATION_REGEX +
	"\\s*(private|protected|public)?[\\s\n]*((static|final|abstract|" +
	"synchronized)[\\s\n]*)*\\s*(private|protected|public)?";

    public static final String SIMPLE_SPACE_REGEX = 
	    "((?<!([,\\&]|super|extends))" +
	    "[\\s\n]+(?!([,\\&]|super|extends)))";

    public static final String CALL_REGEX = 
	    "(abstract|static|final|synchronized)?[\\s\n]*" +
	    "([A-Za-z0-9_]+(<[A-Za-z0-9_\\?\\&,<>\\s]+>)?)[\\s\n]*\\(" +
	    "[^\\(\\)\\{]*" +
	    "\\)[A-Za-z0-9_<>\\s\n]*\\{";

    public static final String CTOR_REGEX = ANNOTATION_REGEX + SCOPE_REGEX + 
            "?" + CALL_REGEX;

    public static final String MEMBER_REGEX = MODIFIER_REGEX + "[\\s\n]*" +
	    "(<[A-Za-z0-9,\\.\\s\\?_]+([\\s\n]+(extends|super)[\\s\n]+" +
	    "[A-Za-z0-9_<>\\s,\\&\\?\\.]+)?>[\\s\n]+)?"+
	    "(void|byte|short|int|long|boolean|" +
	    "[A-Za-z0-9_\\.]*(<.*>)?)[\\[\\]]*" +
	    "[\\s\n]+" +
	    "[A-Za-z0-9_\\.]+[\\s\n]*";

    public static final String METHOD_REGEX = MEMBER_REGEX +
            "\\(([\\s\n]*" + ANNOTATION_REGEX +
            "((abstract|static|final|synchronized)[\\s\n]+)?" +
            "[A-Za-z0-9_\\.\\[\\]\\@\\?]+" +
            "(<[A-Za-z0-9\\@\\s\\?,\\.\\&<>\\[\\]]*>(\\s*[\\.\\[\\]]*))?\\s+"+
             "[A-Za-z0-9_\\!\\.\\@\\?\\[\\]]+\\s*,?)*\\)"+
            "[A-Za-z0-9_,\\.\\&\\[\\]\\s\n]*[\\{;]";

    public static final String FIELD_REGEX = MEMBER_REGEX + "[=;]";


    public Node() { this.clear(); }

    public Node(int location) { this(); this.location = location; }

    public Node(String text) { 
        this();
	this.text = text; 
        this.length = text.length();
        isString = Pattern.matches("^[\"'].*[\"']$", text==null? "": 
			text.replaceAll("\n",""));
    }

    public Node(String text, int loc) { this(text); this.location = loc; }

    public Node(List<Node> nodes) { 
        this.nodes = nodes; 
        this.location = nodes.isEmpty() ? 0 : nodes.get(0).location; 
    }

    public Node(Parameter p) {
        this.name = p.getName();
        this.retval = p.getType().toString();
        put("name", this.name);
        put("retval", this.retval);
    }

    public Node add(Node n) {
	n.parent = this;
	nodes.add(n);
	return n;
    }

    public Node getLast() { 
        return nodes.isEmpty()? null: nodes.get(nodes.size()-1);
    }

    public Node get(int i) {
	return nodes.get(i);
    }

    @Override
    public String table() {
	return "kbase.node";
    }

    public void setExitStatus(List<Integer> exitCodes) {
        if (exitCodes == null) return;
	if (exitCodes.size() == 1) {
	    runs = exitCodes.get(0) != 0;
	} else {
	    compiles = exitCodes.get(0) != 0;
	    runs = exitCodes.get(1) != 0;
	}
    }

    public void setLevel() {
	for (Node node : nodes) {
	    node.level = level + 1;
	    node.setLevel();
	}
    }

    public boolean isFunction() {
	return level==1 && Pattern.matches(
	    "[^\\(\\)\\{\\}]*([^\\)]*)[^\\{]*\\{.*\\}", flatten());
    }

    public List<String> getImports() {
        List<String> imports = new ArrayList();
        for (Node n : nodes) {
            List<String> cimps = n.getImports();
            if (!cimps.isEmpty()) 
                return cimps;
            Matcher m = Pattern.compile("import[\\s]+([^;]*);").matcher(
                n.flatten().replaceAll("\n", " ").trim());
            if (m.matches()) 
                imports.add(m.group(1));
        }
	return imports;
    }

    public String getPackage() {
	if (pkg != null && pkg.length() > 0) return pkg;
	Matcher m = Pattern.compile("package[\\s]+([^;]*);").matcher(flatten());
	if (m.find()) pkg = m.group(1);
        else if (filename != null) {
            String[] toks = filename.split("[\\\\\\/]");
            pkg = filename.replaceAll("[\\\\\\/]", ".").
                      replaceAll("^[^\\.]*\\.", "").replaceAll("[\\-]", "_").
                      replace("." + toks[toks.length-1], "").
                      replaceAll("\\.\\.", ".");
        }
	return pkg;
    }

    public static String getClassName(String classDef) {
        log.trace("getClassName.classDef = " + classDef);
        for (String tok: classDef.replaceAll("\n", " ").replaceAll(
	    ".*[\\s\n]+(class|enum|interface)[\\s\n]+", "").replaceAll(
            "^(class|enum|interface)[\\s\n]+", "").split("[\\s\n]+")) 
            if (tok.length() > 0) 
		return tok.replaceAll("[^A-Za-z0-9_].*", "");
        return null; 
    }

    public String getClassName() {
	if (name == null || name.length()==0) {
            Optional<String> opt = getMatches(CLASS_REGEX).stream().
	        map(Node::getClassName).findFirst();
	    name = opt.isPresent()? opt.get(): null;
	}
        if (classdef == null && parent != null && 
            (name == null || "".equals(name))) 
            name = this.parent.getClassName();
	log.trace("getClassName = " + name );
	return name;
    }

    public String getClassType() {
        if (classtype != null && !"".equals(classtype)) return classtype;
	Optional<String> opt = getMatches(CLASS_REGEX).stream().
            filter(c -> Pattern.matches(".*\\s"+getClassName()+"[\\s\\{<].*", 
                   c.replaceAll("\n", " "))).
	    map(Node::getClassType).findFirst();
	classtype = opt.isPresent()? opt.get(): "";
        if (classdef == null && parent != null && 
            (classtype == null || "".equals(classtype))) 
            classtype = this.parent.getClassType();
        return classtype;
    }

    public static String getClassType(String def) {
	String type = def.replaceAll("[\n]", " ").replaceAll(
	    ".*((abstract\\s+(static\\s+)?)?class|enum|interface)(\\s.*)?$",
	    "$1");
        log.trace("getClassType.def = " + def + ", type = " + type);
        String name = getClassName(def);
        name = name == null ? "" : name;
        return type.trim().equals(name.trim()) ? "" : type.trim();
    }

    public String getClassModifiers() {
        if (modifiers != null && !"".equals(modifiers)) return modifiers;
	Optional<String> opt = getMatches(CLASS_REGEX).stream().
            map(c -> c.replaceAll("\n", " ")).
	    map(Node::getClassModifiers).
	    findFirst();
        modifiers = opt.isPresent()? opt.get(): "";
        if (classdef == null && parent != null && 
            (modifiers == null || "".equals(modifiers))) 
            modifiers = this.parent.getClassModifiers();
        log.trace("getClassModifiers.mods = " + modifiers + ", name = " +  
            getClassName() + ", matches = " + getMatches(CLASS_REGEX));
        return modifiers;
    }

    public static String getClassModifiers(String def) {
        String name = getClassName(def);
	String mod=def.replaceAll(ANNOTATION_REGEX, "").replaceAll("[\n]", " ").
            replaceAll("\\s*(class|enum|interface)(\\s.*)?$", "").
	    replaceAll(ANNOTATION_REGEX, "");
        return name == null ? mod : mod.replaceAll(name, "");
    }

    public String getClassDef() {
        return classdef;
    }

    public String getClassGeneric() {
        if (generic != null && !"".equals(generic)) return generic;
        String inheritance = getClassInheritance();
	Optional<String> opt = (getClassDef() == null ? 
            getMatches(CLASS_REGEX) : Arrays.asList(getClassDef())).stream().
            map(c -> c.replaceAll("\n", " ").replace(inheritance, "")).
            filter(c ->Pattern.matches(".*\\s"+getClassName()+"[\\s<\\{].*",c)).
            filter(c -> Pattern.matches(".*<.*>.*", c)).
	    map(Node::getClassGeneric).
	    findFirst();
	generic = opt.isPresent()? opt.get(): "";
        if (getClassDef() == null && 
            this.parent != null && (generic == null || "".equals(generic))) 
            generic = this.parent.getClassGeneric();
        log.trace("getClassGeneric().generic = " +generic + ", this = " + this);
        return generic;
    }

    private static String getClassGeneric(String def) {
        List<String> toks = splitTopLevel(def, "[<>]");
        log.trace("getClassGeneric.toks = " + toks + ", def = " + def);
        for (int i = 0; i < toks.size(); i++) {
            if (Pattern.matches(".*(class|enum|interface)\\s+[A-Za-z0-9_]+\\s*",
                toks.get(i).replaceAll("\n", " ")) && i < toks.size()-1)
                return "<" + toks.get(i+1) + ">";
        }
        log.trace("getClassGeneric.return null");
        return ""; 
    }

    public String getClassInheritance() {
        if (inherit != null && !"".equals(inherit)) return inherit;
	Optional<String> opt = 
            getClassDefs().stream().
            map(c -> c.replaceAll("\n", " ")).
            filter(c ->Pattern.matches(".*\\s"+getClassName()+"[\\s<\\{].*",c)).
            filter(c -> matchesTopLevel("(extends|implements)", c)).
	    map(Node::getClassInheritance).
            filter(c -> c.trim().length()>0).findFirst();
	inherit = opt.isPresent()? opt.get(): "";
        if (classdef == null && parent != null && 
            (inherit == null || "".equals(inherit))) 
            inherit = parent.getClassInheritance();
        log.trace("getClassInheritance.inherit = " + inherit + 
                 ", name = " + getClassName());
        return inherit;
    }

    public static String getClassInheritance(String def) {
        def = def.replaceAll("\n", " ");
        if (!matchesTopLevel("(extends|implements)", def)) return null;
        return def.substring(
            splitTopLevel(def, "(extends|implements)").get(0).length(),
            def.trim().endsWith("{")? def.lastIndexOf("{") : def.length());
    }

    public List<String> getClassAnnotations() {
	if (annotations != null) return annotations; 
	aggregates = null;
	log.trace("getClassAnnotations.className = " + getClassName());
	List<List<String>> matches = getClassDefs(0, flatten().length(), true).
	    stream().filter(c -> Pattern.matches(".*"+getClassName()+".*", 
					c.replaceAll("\n", ""))).
	    map(Node::getClassAnnotations).collect(Collectors.toList());
	annotations = new ArrayList();
	for (List<String> annos : matches) 
	   for (String anno : annos)
	       if (!annotations.contains(anno))
		   annotations.add(anno);
        if (classdef == null && this.parent != null && (annotations.isEmpty())) 
            annotations = this.parent.getClassAnnotations();
	return annotations;
    }

    public static List<String> getClassAnnotations(String classDef) {
	List<String> annos = new ArrayList();
	if (!classDef.contains("@")) return annos;
	List<String> splits = splitTopLevel(classDef, '(', ')', "\\@");
	log.trace("getClassAnnotations.classDef ="+classDef+", splits="+splits);
	for (int i = 0; i < splits.size(); i++) {
	    String split = splits.get(i);
	    if (i == splits.size() - 1) {
		int j = split.contains("\n") && split.indexOf("\n") < split.
		    indexOf(" ") ? split.indexOf("\n") : split.indexOf(" ");
		if (split.contains("(") && split.indexOf("(") < j) {
		    List<String> toks = splitTopLevel(split, '(', ')', null);
		    split = toks.get(0) + "(" + toks.get(1) + ")";
		} else {
		    String[] toks = split.split("[\\s\n]");
		    if (toks.length>0) split = toks[0];
		}
	    } 
	    if (split.trim().length() > 0) 
		annos.add((classDef.contains("@"+split) ? "@" : "") + split);
	}
	log.trace("getClassAnnotations.annotations = " + annos);
	return annos;
    }

    public Map<Node,String> getMatchNodes(String regex, int start, int length) {
	String text = flatten(true, false, null, true, true);
	flattened = null;
	log.trace("getMatchesNodes.text = " + text.length() + ", start = " + 
	    start + ", length = " + length);
	if (!matchNodeMap.containsKey(regex)) {
	    int i = 0; 
	    matchNodeMap.put(regex, new HashMap());
	    Matcher m = Pattern.compile(regex).matcher(text);
	    while (m.find()) {
		i = i + text.substring(i).indexOf(m.group());
		Node n = find(location + i + 1);
		i += m.group().length();
		log.trace("getMatchNodes.group = " + m.group() + ", n = " + n);
		if (!n.isComment && !n.isString)
		    matchNodeMap.get(regex).put(n, m.group());
	    }
	}
	Map<Node, String> matchNodes = 
	    matchNodeMap.get(regex).entrySet().stream().filter(ent ->
	    ent.getKey().location >= start && ent.getKey().location +
	    ent.getKey().getLength() <= start + length).
	    collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
	return matchNodes;
    }

    public List<String> getMatches(String regex) {
	return getMatches(regex, location, flatten().length());
    }

    public List<String> getMatches(String regex, int start, int length) {
	List<String> matches = new ArrayList();
	String text = flatten(true, false, null, true, true);
	flattened = null;
	if (start >= text.length()) return matches;
	String search = start + length > text.length()? text.substring(start):
		text.substring(start, start + length);
	log.trace("getMatches.search = " + search + ", regex = " + regex);
	Matcher m = Pattern.compile(regex).matcher(search);
	int i = start;
	while (m.find()) {
	    i += text.substring(i).indexOf(m.group().trim()) + 1;
	    Node n = find(this.location + i);
	    i += m.group().trim().length();
	    log.trace("getMatches.m.group="+m.group()+", i=" + i + ", n=" + n); 
	    if (n == null || n.isComment || n.isString) continue;
	    Node n1 = find(this.location + i - 1);
	    if (n1 != null && !n1.isComment && !n.isString) 
		 matches.add(m.group());
	}
	log.trace("getMatches.matches = " + matches);
	return matches;
    }

    public List<String> getMatchesRecursive(String regex, int start, 
	    int length) { // TODO: finish testing
	List<String> matches = new ArrayList();
	String text = flatten(false, false, null, true, true);
	String search = text.substring(start, start + length);
	List<Map<Integer, String>> insts = new ArrayList();
	List<String> reg = setMarkers(regex, search, "\\[N\\]", insts);
	regex = reg.get(0); search = reg.get(1);
	Matcher m = Pattern.compile(regex).matcher(search);
	int i = 0; insts.add(new HashMap());
	while (m.find()) {
	    i = i + search.substring(i).indexOf(m.group());
	    Node n = find(i, m.group().length());
	    if (!n.isComment && !n.isString) 
		insts.get(insts.size()-1).put(i, m.group());
	}
	replaceMarker(matches, "\\[N\\]", insts);
	log.trace("getMatches.matches = " + matches);
	return matches;
    }

    private String clearInstances(String search, String regex, 
	    List<Map<Integer, String>> recurs) {
	Matcher m1 = Pattern.compile(regex).matcher(search);
	int i = -1;
	recurs.add(new HashMap());
	log.trace("clearInstances.search.b4 = " + search.length() + ", regex = " + regex);
	while (m1.find()) {
	    i = ++i + search.substring(i).indexOf(m1.group());
	    recurs.get(recurs.size()-1).put(i, m1.group());
	}
	search = search.replaceAll(regex, "[N]");
	 log.trace("clearInstances.search.after = " + search.length());
	return search;
    }

    private List<String> setMarkers(String regex, String search, String marker, 
	    List<Map<Integer, String>> insts) {
	// TODO: custom recursive match: [N|...] -> [N1] construct
	Matcher m = Pattern.compile(
	    "\\[N1\\(((?!\\[N).+)\\[N\\|([^\\]]*)\\]((?!N\\]).*)N1\\]").
	    matcher(regex);
	log.trace("setMarkers.regex.b4 = " + regex);
	if (m.find()) {
	    log.trace("setMarkers.grp1=" + m.group(1) + ", grp2=" + m.group(2) +
		    ", grp3=" + m.group(3));
	    search = clearInstances(search, "["+m.group(2)+"]", insts);
	    String subReg = m.group(1) + marker + m.group(3);
	    while (Pattern.matches(subReg, search))
		search = clearInstances(search, subReg, insts);
	    regex = regex.replaceAll(m.group(), marker);
	    log.trace("setMarkers.regex = " + regex );
	}
	return Arrays.asList(regex, search);
    }

    private void replaceMarker(List<String> matches, String marker,
	    List<Map<Integer,String>> insts) {
	int cnt = insts.size() - 1;
	matches.addAll(insts.get(cnt).values());
	if (matches.size()>0 && matches.get(0).contains(marker)) {
	    matches.clear();
	    for (Map.Entry<Integer, String> m : insts.get(cnt).entrySet()) {
		String match = m.getValue();
		for (int j = cnt; j >= 0; j--) {
		    int i = m.getKey() + match.indexOf(marker);
		    match = match.replaceAll(marker, insts.get(j).get(i));
		}
		matches.add(match);
	    }
	} 
    }

    public List<String> getFields() {
	if (fields != null) return fields;
	String text = flatten();
	fields = getFields(0, text.length());
	return fields;
    }

    private List<String> getFields(int start, int length) {
	Node node = find(start, length);
	node = (node == null ? this : node);
        if (!node.nodes.isEmpty() && 
            node.matchTop("class","interface","enum","@interface") &&
            "}".equals(node.nodes.get(node.nodes.size()-1).end))
            node = node.nodes.get(node.nodes.size() - 1);
	boolean classOk = false;
	List<String> res = node.aggregate(true, true).stream().
	    map(l -> l.stream().filter(n -> !n.isComment).
			       collect(Collectors.toList())).
	    map(l -> !l.isEmpty() && !(new Node(l).matchTop("=") ||
		     isField(new Node(l).flatten())) &&
		"}".equals(l.get(l.size()-1).end)? l.subList(0,l.size()-1): l).
	    map(l -> new Node(l)).
	    filter(n -> chckNull(n.matchTop("class","interface","enum",
		"@interface"),classOk)).
	    map(n ->n.flatten(true, true)).collect(Collectors.toList());
	List<String> members = groupClassMembers(res);
        log.trace("getFields.members = " +members+ ", size = "+members.size());
	List<String> txts = members.stream().filter(s -> isField(s)).
	    collect(Collectors.toList());
	if (!txts.isEmpty() && txts.get(txts.size()-1).endsWith(","))
	    txts.add(members.get(members.size()-1));
	log.trace("getFields.flds = " + txts + ", node = " + node);
        return txts;
    }

    private static boolean isField(String s) {
        s = s.replaceAll("(^[,;:]*\\s*|\\s*$)", "");
        if (s.endsWith(";") || s.endsWith(",") &&
            !Arrays.asList(s.split("\\s")).contains("throws") ||
            s.startsWith(";") || s.startsWith(",") || s.trim().equals("static"))
            return true; 
        s = s.substring(0, s.contains("{")? s.indexOf("{"): s.length()).trim();
        return Pattern.matches("^[A-Za-z0-9_\\[\\]=\\.<>\\s]+$", s); // check if is enum
    }

    private boolean isField(Node n) {
        return getFields().contains(n.flatten()) || 
               getFields().contains(n.flatten() + ";");
    }

    private static List<String> groupClassMemberTexts(List<Node> members) {
        Map<String, Node> memNodes = new HashMap();
        List<String> keys = new ArrayList();
        for (Node mem : members) {
            keys.add(mem.flatten(true,true,null,false,false,true));
            memNodes.put(keys.get(keys.size()-1), mem);
        }
        List<String> txts = new ArrayList();
        int i = 0;
        for (String txt : groupClassMembers(keys)) {
            StringBuilder group = new StringBuilder();
            log.trace("groupClassMemberTexts().txt = " + txt + ", i = " + i +
                      " out of " + keys.size() +
                      ", key = " + (i<keys.size() ? keys.get(i) : ""));
            while (i < keys.size() && !"".equals(txt) &&
                   (txt.contains(keys.get(i)) || keys.get(i).contains(txt))) {
                txt = keys.get(i).contains(txt) ? "" :
                      txt.substring(txt.indexOf(keys.get(i)) + 
                                    keys.get(i).length());
                group.append(memNodes.get(keys.get(i++)).flatten(true, true));
            }
            if (group.toString().trim().equals(";") && !txts.isEmpty())
                txts.set(txts.size() - 1,
                         txts.get(txts.size() - 1) + group.toString());
            else
                txts.add(group.toString());
            log.trace("groupClassMemberTexts().group = " + group);
        }
        return txts;
    }

    private static List<String> groupClassMembers(List<String> strs) {
        List<String> txts = new ArrayList();
        String fld = "";
        log.trace("groupClassMembers.strs = " + strs);
        for (String txt : strs) {
            fld += txt;
            String s = txt.replaceAll("\n", " ").trim();
            log.trace("groupClassMembers.s = " + s);
            if (Pattern.matches("(i?)(^|.*\\s)(class|enum|interface).*", s) ||
                Pattern.matches(".*[,;]$", s) ||
                Pattern.matches("^static(\\s.*|$)", s) || 
                Pattern.matches(METHOD_REGEX, s+" {") ||
                Pattern.matches(CTOR_REGEX, s+" {")) {
                txts.add(fld); 
                fld = ""; 
            }
            if (!fld.equals(txt) && fld.trim().length() > 0) {
                txts.add(fld.substring(0, fld.length() - txt.length()));
                if (txt.length() > 0) txts.add(txt);
                fld = "";
            }
            groupSemicolon(txts);
        }
        if (fld.length() > 0) txts.add(fld);
        groupSemicolon(txts);
        log.trace("groupClassMembers().strs = " + strs + ", txts = " + txts);
        return txts;
    }

    private static void groupSemicolon(List<String> txts) {
        if (txts.size()>1 && txts.get(txts.size()-1).trim().equals(";")) {
            txts.remove(txts.size()-1);
            txts.set(txts.size() - 1, txts.get(txts.size() - 1) + ";");
        }
    }

    private List<String> getMembers(int start, int length, boolean getFields) {
        List<String> txts = new ArrayList();
        List<Node> cs = getClassNodes(start, length, true);
        if (!cs.isEmpty()) {
            for (Node n : cs.get(0).nodes) 
                if (!"}".equals(n.end)) 
                    for (Node n1 : n.nodes) 
                        if (!getFields && !";".equals(n.end) || 
                             getFields && ";".equals(n1.end)) 
                             txts.add(n1.flatten());
        }
        log.trace("getMembers.getFields = " + getFields + 
            ", txts = " +txts+ ", cs = " +cs+ ", this = " + this.name);
        return txts;
    }

    private List<Node> getClassNodes(int start, int length, boolean getFirst) {
        Node node = find(start < 1 ? 1 : start, length); 
        node = (node == null ? this : node.find("", "}"));
        node = (node == null ? this : node);
        List<Node> cs = new ArrayList();
        Node p = null, c = null;
        for (List<Node> l : node.aggregate(start > 0, true)) {
          if (c != null) break;
          for (Node n : l) {
             if (p != null && p.findAny("class", "enum", "interface")!=null) {
                 cs.add(n);
                 if (getFirst) return cs;
             }
             p = n;
          } 
        }
        return cs;
    }

    private List<String> toStatements(List<String> txts) {
        //TODO: implement a proper solution in aggregate() and remove this
        List<String> res = new ArrayList();
        for (String fld : txts) {
            if (fld.contains(";"))
                res.addAll(Arrays.asList(fld.split(";")));
            else res.add(fld);
        }
        return res;
    }

    public List<String> getConstructors() {
        if (ctors == null) {
	    ctors = getMethods(location, flatten().length()).stream().
                filter(m ->Pattern.matches(CTOR_REGEX, m)).
                collect(Collectors.toList());
	    //ctors = getConstructors(location, flatten().length());
        }
        return ctors;
    }

    private List<String> getConstructors(int start, int length) {
	log.trace("getConstructors start=" + start + ", len=" + length);
        List<String> ctors = getMatches(CTOR_REGEX, start, length);
        if (getClassName() != null && !"".equals(getClassName().trim()))
            ctors.addAll(getMatches("(?<=[;\\{\\}])[\\s\n]*" + getClassName() +
                "[\\s\n]*\\([^\\)]*\\)[^\\{]*\\{", start, length));
        log.trace("getConstructors.ctors = " + ctors);
        return ctors;
    }

    private List<String> getInstantiations(int start, int length) {
	    log.trace("getInstantiations start=" + start + ", len=" + length);
        return getMatches("new[\\s\n]*"+
	    "([A-Za-z0-9_\\?\\&,<>\\s]+)[\\s\n]*\\(" +
	    "[^\\(\\)\\{]*" +
	    "\\)[\\s\n]*\\{",
	    start, length);
    }

    public static String getMethodRegex() {
        return METHOD_REGEX;
    }

    public List<String> getMethods() {
        if (methods == null) 
	    methods = getMethods(location, flatten().length());
        log.trace("getMethods.0.meths = " + methods + ", classname = " + name);
        return methods;
    }

    private List<String> getMethods(int start, int length) {
        return getClassDefs(start == 0 ? 1 : start, length, false);
    }

    private List<String> getClassDefsFiltered(int start, int length, 
        boolean classOk, boolean wStmts) {
        List<String> defs = 
            getClassDefs(start == 0 ? 1 : start, length, classOk).stream().
            map(s -> wStmts ? s.replaceAll("[^;]*$", "") : 
                                 s.replaceAll("[^;]*;", "")).
            filter(s -> s.trim().length() > 0).
            collect(Collectors.toList());
        if (wStmts) {
            List<String> txts = new ArrayList();
            for (String s : defs) 
                for (String tok : s.split(";")) 
                    txts.add(tok + ";");
            defs = txts;
        }
        log.trace("getClassDefsFiltered.wStmts = " +wStmts+ ", defs = " +defs+ 
                 ", this = " + this);
        return defs;
    }

    public List<String> getClassDefs() {
        if (classdefs == null) 
	    classdefs = getClassDefs(location, flatten().length(), true);
        if (this.parent != null && (classdefs == null || classdefs.isEmpty())) 
	    classdefs = this.parent.getClassDefs(location, flatten().length(),
                                                 true);
        log.trace("getClassDefs.0.defs = " +classdefs+ ", classname = " + name);
        return classdefs;
    }

    public List<String> getClassDefs(int start, int length, boolean classOk) {
	log.trace("getClassDefs start=" +start+ ", len=" + length +","+classOk);
        Node node = find(start, length); 
        node = (node == null ? this : node);
        aggregates = null;
        List<String> defs = node.aggregate(start > 0).stream().
           filter(l -> !l.isEmpty() && "}".equals(l.get(l.size()-1).end) &&
               (classOk ||
                l.stream().filter(n->")".equals(n.end)).findAny().isPresent())).
           filter(l -> !l.isEmpty() && "}".equals(l.get(l.size()-1).end)).
           map(l -> l.stream().filter(n -> !n.isComment && !n.isString).
                               collect(Collectors.toList())).
           map(l -> new Node(!"}".equals(l.get(l.size()-1).end) ? l :
                              l.subList(0, l.size()-1))).
           filter(n ->chckNull(n.matchTop("class","interface","enum",
                "@interface"),classOk)).
           filter(n ->chckNull(n.matchTop("="), false)).
           map(n -> n.flatten(true, true)).collect(Collectors.toList());
        log.trace("getClassDefs.defs = " + defs + ", this = " + this.name);
        return defs;
    }

    public Node getClassDefNode(String def) {
        int start = location, length = flatten().length();
        boolean classOk = true;
        Node node = this;
        aggregates = null;
        List<Node> defs = node.aggregate(start > 0).stream().
           filter(l -> !l.isEmpty() && "}".equals(l.get(l.size()-1).end)).
           map(l -> new Node(l.stream().filter(n -> !n.isComment && !n.isString).
                               collect(Collectors.toList()))).
           filter(n ->chckNull(n.matchTop("class","interface","enum", 
               "@interface"), classOk)).
           filter(n -> n.flatten(true, true).contains(def)).
           map(n -> n.nodes.get(n.nodes.size()-1)).collect(Collectors.toList());
        log.trace("getClassDefNode.defs = " + defs + ", this = " + this.name);
        return defs.get(0);
    }


    public static boolean chckNull(Object o, boolean nonNull) {
        return nonNull ? o==Boolean.TRUE || !(o instanceof Boolean) && o!=null:
                         o == null || o == Boolean.FALSE;
    }

    public List<Node> getAggregate(String content) {
        List<Node> defs = aggregate(true, true).stream().
           map(l -> l.stream().filter(n -> !n.isComment && !n.isString).
                      collect(Collectors.toList())).
           filter(l ->!l.isEmpty()).
           filter(l -> new Node(!"}".equals(l.get(l.size()-1).end) ? l :
               l.subList(0,l.size()-1)).flatten(true, true).trim().
               contains(content.trim())).
           map(l -> new Node(l)).
           collect(Collectors.toList());
        List<String> defs1 = aggregate(true).stream().
           map(l -> l.stream().filter(n -> !n.isComment && !n.isString).
                      collect(Collectors.toList())).
           filter(l ->!l.isEmpty()).
           map(l -> new Node(l).flatten(true,true)).
           collect(Collectors.toList());
        log.trace("getAggregate.defs = " + defs +", content = " + content + 
                 ", defs1 = " + defs1 + ", this = " + this.name);
        return defs;
    }
 
    public <T> List<T> removeInners(Function<Node, List<T>> getFn) {
        List<T> matches = getFn.apply(this);
        log.trace("removeInners.matches.b4 = " + matches);
        for (Node n : getInnerClasses()) 
            if (n.name != null && !n.name.equals(name))
                for (T match : getFn.apply(n))
                    if (matches.contains(match))
                        matches.remove(matches.indexOf(match));
        log.trace("removeInners.matches = " + matches);
        return matches;
    }

    public Set<String> getMethodTypes() {
	return getMethodTypes(0, flatten().length()); 
    }

    public Set<String> getMethodTypes(int start, int length) {
	Set<String> types = new HashSet();
	if (methodTypes != null) {
	    log.trace("getMethodTypes.cached.types = " + methodTypes);
	    return methodTypes;
        }
	int i = start; 
	for (String method : concat(getMethods(), 
		    getConstructors())) {
	    types.addAll(getMethodTypes(method));
	}
	methodTypes = types;
	log.trace("getMethodTypes.types = " + types);
	return types;
    }

    public List<String> getMethodTypes(String method) {
	List<String> types = new ArrayList();
	String[] toks = splitMethod(method);
	String text = flatten();
	int i = location + text.indexOf(method) + toks[0].length() + 1;
	Node params = find(i, ")");
	log.trace("getMethodTypes.i = " + i + ", params = " + params + 
            ", asParameters = " + (params == null? null: params.asParameters())+
            ", method = " + method);
	if (params != null)
	    types.addAll(params.asParameters().stream().map(p -> 
		    p.getDeclTypes().toArray(new String[0])).map(Stream::of).
		reduce(Stream.of(new String[0]), Stream::concat).
		collect(Collectors.toList()));
	toks = splitTopLevel(toks[0], "[\\s\n]").toArray(new String[0]); 
	if (toks.length > 1 && !"?".equals(toks[toks.length - 2].length()))
	    types.addAll(getDeclTypes(toks[toks.length - 2] + " var"));
	types = types.stream().filter(t -> t!=null && t.length() > 0).
	    collect(Collectors.toList());
	log.trace("getMethodTypes.types = " + types + ":" + types.size() + 
                  ", mclass = " + getClassName());
	return types;
    }

    private String toParametrizedType(String type, int paramIndex) {
	Node b = find(paramIndex);
	List<Integer> indices = new ArrayList();
	if (b != null && !b.isComment && !b.isString) 
	    for (Node i : b.nodes) 
		if (!i.isComment && !i.isString) 
		    indices.add(indices.size());
	return type + "<" + (indices.isEmpty()? "T":
	    indices.stream().map(
		i -> "T" + i).collect(
	    Collectors.joining(", "))) + ">";
    }

    private boolean isAbstract(String classOrMethod) {
        return Pattern.matches(
	    "(.|\n)*(abstract(.(?!implements))*extends|final)" +
	    "((.|\n)(?!implements))*[\\{\\(][\\s\n]*", 
			classOrMethod);
    }

    public static boolean matchesTopLevel(String regex, String str) {
        return splitTopLevel(str, regex).size() > 1;
    }

    public static List<String> splitTopLevel(List<String> strs, String regex) {
        List<String> toks = new ArrayList();
	for (String s : strs) toks.addAll(splitTopLevel(s, regex));
	return toks;
    }

    public static List<String> splitTopLevel(String str, String regex) {
        List<String> splits = new ArrayList();
	int unclosed = 0, h = 0, open = 0, close = 0;
        boolean[] inStr = new boolean[]{ false, false };
	for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '<') open++;
            else if (str.charAt(i) == '>') close++;
        }
	for (int i = 0; i < str.length(); i++) {
            inStr = isQuoted(str, i, inStr[0], inStr[1]);
            if (!inStr[0] && str.charAt(i) == '<') open--;
            else if (!inStr[0] && str.charAt(i) == '>') close--;
	    if (!inStr[0] && str.charAt(i) == '<' && close > open) unclosed++;
	    else if (!inStr[0] && str.charAt(i)=='>' && unclosed>0) unclosed--;
	    String s = str.substring(i);
	    if ((unclosed == 1 && str.charAt(i) == '<' || 
		 unclosed == 0 && i >= h) && !inStr[0] &&
	        Pattern.matches(regex + ".*", s.replaceAll("\n", ""))) {
		splits.add(str.substring(h, i));
		h = i + s.length() - s.replaceFirst(regex, "").length();
	    }
	}
	if (splits.isEmpty()) splits.add(str);
	else splits.add(str.substring(h));
	return splits;
    }

    static boolean[] isQuoted(String str, int i, boolean inStr, 
                                  boolean isDquote) {
        for (char quote : Arrays.asList('"', '\'')) {
          if (str.charAt(i) == quote && (!inStr || 
              isDquote && quote=='"' || !isDquote && quote=='\'') &&
              !str.substring(0,i).endsWith("\\")) {
            inStr = inStr ? false : true; 
            isDquote = quote == '"';
          }
        }
        return new boolean[] { inStr, isDquote };
    }

    public static List<String> splitTopLevel(String str, char open, char close,
        String regex) {
        List<String> splits = new ArrayList();
	int unclosed = 0, unclosedReg = 0, h = 0;
        boolean[] inStr = new boolean[]{ false, false };
	for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            inStr = isQuoted(str, i, inStr[0], inStr[1]);
            if (inStr[0]) continue;
	    if (c == '<') unclosed++;
	    else if (c == '>' && unclosed>0) unclosed--;
	    if (c == open) unclosedReg++;
	    else if (c == close && unclosedReg>0) unclosedReg--;
	    String s = str.substring(i);
	    if (unclosed == 0 && i >= h && (unclosedReg == 0 && regex != null &&
                Pattern.matches(regex + ".*", s.replaceAll("\n", " ")) ||
                regex == null &&
                (unclosedReg==1 && c==open || unclosedReg==0 && c==close))) {
		splits.add(str.substring(h, i));
		if (regex != null)
                    h = i + s.length() - s.replaceFirst(regex, "").length();
                else h = i + 1;
	    }
	}
	if (splits.isEmpty()) splits.add(str);
	else splits.add(str.substring(h));
	return splits;
    }

    public static String[] splitMethod(String method) {
        if (method.trim().endsWith(";")) {
            String[] tks = method.replaceAll(";[\n\\s]*$","").split(";");
            method = tks[tks.length-1];
        }
        List<String> splits = splitTopLevel(method, '(', ')', null);
        log.trace("splitMethod().splits = " + splits);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < splits.size() - 2; i++) {
            if (sb.length() > 0) sb.append(i % 2 == 1 ? "(" : ")");
            sb.append(splits.get(i));
        }
        if (splits.size() == 1)
            sb.append(splits.get(0));
        log.trace("splitMethod.sb = " + sb.toString() + ", method = " + method);
        return new String[] { sb.toString(), splits.size()<2 ? "" : 
            splits.get(splits.size() - 2), splits.size()<1 ? "" :
            splits.get(splits.size() - 1) }; 
    }

    public static Map<List<Integer>, String> splitTopLevelMap(
	List<String> strs, String regex) {
        Map<List<Integer>, String> toks = new LinkedHashMap();
	int pos = 0;
	for (String s : strs) {
	    int i = 0, prev = 0;
	    for (String tok : splitTopLevel(s, regex)) {
	        if (tok != null && tok.length() > 0)  {
		    prev = i;
		    i += s.substring(i).indexOf(tok);
		    toks.put(Arrays.asList( pos, i, prev ), tok);
		    i += tok.length();
		}
	    }
	    pos++;
	}
	return toks;
    }

    public String trimBalanced(String str) {
	int unclosed = 0, h = 0;
	for (int i = 0; i < str.length(); i++) {
	    int prev = unclosed;
	    if (str.charAt(i) == '<') unclosed++;
	    else if (str.charAt(i) == '>') unclosed--;
	    if (prev == 0 && prev != unclosed) h = i;
	    if (unclosed < 0) return str.substring(0, h);
	}
	return str.substring(0, unclosed==0? str.length(): h).trim();
    }

    public Map<String, String> getParametrizedMethodTypes() {
	return getParametrizedMethodTypes(0, flatten().length());
    }

    private Map<String, String> getParametrizedMethodTypes(int start, int len) {
        Map<String, String> types = new HashMap();
	String text = this.flatten().substring(start, start + len);
        Matcher m = Pattern.compile(
	    "((.(?!(extends|implements|super)))*)[\\s\n]+" +
	    "(extends|implements|super)[\\s\n]+" +
	    "([A-Za-z0-9_\\s,\\&\\?<>\\.]+)").matcher(text);
	while (m.find()) {
	    String sup = m.group(5), reln = m.group(4);
	    log.trace("getParametrized.m.group = " + m.group() + ", start = " + 
                start + ", len = " + len + ", newstart = " + 
                (start + text.indexOf(m.group()) + m.group().indexOf(sup)) + 
                ", newlen = " + sup.length() + ", sup = " + sup + ", reln = " +                 reln + ", types = " + types);
	    for (Map.Entry<String, String> ent : getParametrizedMethodTypes(
		start + text.indexOf(m.group()) + m.group().indexOf(sup), 
		sup.length()).entrySet())
		types.putIfAbsent(ent.getKey(), ent.getValue());
	    sup = trimBalanced(sup.replaceAll(",[\\s\n]*$", ""));
	    List<String> splits = splitTopLevel(sup, 
			    "(extends|implements|super)");
	    if (splits.size() > 1) sup = splits.get(0);
	    boolean isClassDef = Pattern.matches(".*(class|interface).*", 
			    m.group(1));
	    reln = m.group(1).contains("interface")? "implements": 
		    (Pattern.matches(".*(abstract|final).*class.*extends.*",
		        m.group(1))?  "extends": reln);
	    if (isClassDef && "extends".equals(reln)) 
		reln = "classextends";
	    Node n = find(location + start + text.indexOf(m.group()) +
			    m.group().indexOf(sup));
	    log.trace("getParametrized.group = " + m.group() + ", groupcount = " + m.groupCount() + ", sup = " + sup + ", reln = " + reln + ", isAbstract = " + isAbstract(m.group()) + ", group(5) = " + m.group(5) + ", location = " + location + ", find.i = " + (location + start + text.indexOf(m.group()) + m.group().indexOf(sup)) + ", isComment = " + (n==null? "null": n.isComment) + ", this = " + this.name + ", text.len = " + text.length());
	    if (n.isComment || n.isString) continue;
	    for (String tok : splitTopLevel(sup, "[,\\&]")) {
		tok = tok.trim();
		if (tok.contains("<")) {
		    n = find(location + start + text.indexOf(m.group()) + 
			m.group().lastIndexOf(tok) + tok.indexOf("<") + 2, ">");
		    log.trace("getParametrized.n = " + n + ", tok = " + tok +
			", i = " + (location + start + text.indexOf(m.group()) +
			m.group().lastIndexOf(tok) + tok.indexOf("<")));
		    tok = tok.substring(0, tok.indexOf("<"));
		    for (String type : getDeclTypes(tok+n.flatten()+ " var")) {
			if (isClassDef && !"classextends".equals(
				types.get(type.trim())))
			    types.remove(type.trim());
			    //removePrefix(types, substring(type, "<"));
			types.putIfAbsent(type.trim().replaceAll("\\.{3}", ""),
			    isAbstract(m.group())? "abstract": 
			    (!type.startsWith(tok)? "": reln));
			log.trace("getParametrized.type1 = " +type+ ", val = " +
                                  types.get(type.trim()) + ", tok = " + tok);
		    }
		} else if (!Pattern.matches(
			    ".*(extends|super|implements|\\?).*", tok)) {
		    tok = tok.trim().replaceAll("\\.{3}", "");
		    log.trace("getParametrized.tok = " + tok);
		    if (isClassDef && !"classextends".equals(
			types.get(tok.trim())))
			    //removePrefix(types, substring(tok, "<"));
		        types.remove(tok.trim());
		    types.putIfAbsent(tok.trim(), isAbstract(m.group())?
			"abstract": reln);
			log.trace("getParametrized.type2 = " + tok.trim() + ", val = " + types.get(tok.trim()));
		}
	    }
	    log.trace("getParametrized.while.types = " + types);
	}
	log.trace("getParametrized.b4.method.types = " + types);
	for (String type : getMethodTypes(start, len)) 
	    if (Pattern.matches("^[A-Z\\@].*", type)) 
		//if (!type.contains("<") || 
		  //  !hasPrefix(types, substring(type, "<")))
	            types.putIfAbsent(type, "");
	for (Map.Entry<String, String> ent : 
	    new ArrayList<Map.Entry<String, String>>(types.entrySet())) {
	    types.remove(ent.getKey());
	    types.put(ent.getKey().replaceAll("(\\[.*\\]|\\.{3})", ""), 
	        ent.getValue());
	}
        log.trace("getParametrized.types = " + types + ", pclass = " + getClassName());
	return types;
    }

    private <T> String keyPrefix(Map<String, T> map, String pre) {
	for (String key : map.keySet())
	    if (key.startsWith(pre)) return key;
	return null;
    }

    private <T> boolean hasPrefix(Map<String, T> map, String pre) {
	return keyPrefix(map, pre) != null;
    }

    private <T> boolean removePrefix(Map<String, T> map, String pre) {
	String key = keyPrefix(map, pre);
	if (key != null) map.remove(key);
	return key != null;
    }

    private String substring(String str, String delim) {
	return str.contains(delim)? str.substring(0, str.indexOf(delim)): str;
    }

    private boolean isModifier(String tok) {
	return Pattern.matches("(final|abstract|<.*>|static|synchronized|" +
		"public|private|protected)", tok);
    }

    public List<String> getDeclTypes() {
	return getDeclTypes(flatten());
    }

    public List<String> getDeclTypes(String str) {
	List<String> types = new ArrayList();
	String[] toks = splitTopLevel(str, "[\\s\n]").toArray(new String[0]);
	String all = flatten();
	log.trace("getDeclTypes.toks = " + Arrays.asList(toks) +":"+toks.length + ", str = " + str);
	for (String tok : toks) {
	    if (!isModifier(tok) && 
	        !Pattern.matches("([\\?\\@]|.*[\\(\\)].*)", tok)) { 
                // TODO: use proper filtering
		if (Pattern.matches(".*<.*>.*", tok)) {
		    int i = tok.indexOf("<");
		    Node n = find(tok.substring(i, tok.lastIndexOf(">")+1),">"); 
		    log.trace("getDeclTypes.n = " + n + ", index = " + (location + all.indexOf(tok) + i + 1) + ", all = " + all.length() + ", index = " + all.indexOf(tok) + ", tok = " + tok + ", search = " + tok.substring(i, tok.lastIndexOf(">")+1));
		    tok = tok.substring(0, i) + "<"; i = 0;
		    if (n.flatten().trim().length() > 0) 
			 for (Node p : n.nodes)
			     types.addAll(getDeclTypes(p.flatten() + " var"));
		    if (n.nodes.isEmpty() || !",".equals(n.nodes.get(0).end)) {
			tok += "T0";
		    } else {
			for (Node p : n.nodes) {
			    if (!"<".equals(p.begin)) {
		                String subStr = p.flatten().
				    replaceAll(",$", "") + " var";
			        if (!subStr.equals(str)) 
			            types.addAll(getDeclTypes(subStr));
			        tok += (i == 0? "": ", ") + "T" + i++;
			    }
			}
		    }
		    tok += ">";
		} 
		tok = tok.replaceAll(".*(extends|implements|super)[\\s\n]*", "")
				.replaceAll("[,]$", "");
	        if (tok != null && tok.length()>0) 
		    types.addAll(Arrays.asList(tok.split("[\\&]")));
	    }
	}
	if (!types.isEmpty()) types.remove(types.size()-1);
	types.remove("?");
	log.trace("getDeclTypes.types = " + types + ", unique = " + 
            (new HashSet(types)) + " : " + types.size() + ", str = " + str);
	return new ArrayList(new HashSet(types));
    }

    private String getReturn(String method) {
	List<String> types = getMethodTypes(method);
	log.trace("getReturn.method = " + method + ", types = " + types);
	return types.isEmpty()? "" : defaultValue(types.get(types.size()-1));
    }

    /*TODO: multi-level flatten, such that node from 2 levels down
     * can get flattened on current level, rather than child level, i.e.
     * flatten child nodes tree into list FIRST, then convert to text */
    public String flatten() {
	return flatten(false);
    }

    public String flattenTree() {
	Node node = this;
	if (this.parent == null) 
	    return node.begin + node.text + node.end;
	StringBuilder sb = new StringBuilder(node.begin).append(node.text);
	List<Node> flatTree = new ArrayList(node.flattenNodes());
	flatTree.sort(new Comparator<Node>() {
	    public int compare(Node a, Node b) { return a.location-b.location;}
	});
	int location = this.location < 0? 0: this.location;
	for (Node n : flatTree) {
	    if (n.location < location + sb.length() && n.location >= location)
		sb.insert(n.location-location, n.flattenTree());
	    else sb.append(n.flattenTree());
	}
	flattened = sb.append(node.end).toString();
	return flattened;
    }

    public String flatten(boolean force) {
        return flatten(force, false);
    }

    public String flatten(boolean force, boolean ignoreComments) {
        return flatten(force, ignoreComments, null);
    }

    public String flatten(boolean force, boolean ignoreComments, String endEx) {
        return flatten(force, ignoreComments, endEx, false, false);
    }

    public String flatten(boolean force, boolean ignoreComments, String endEx,
                          boolean blankStrs, boolean blankCmmts) {
        return flatten(force, ignoreComments, endEx, blankStrs, blankCmmts, 
                       false);
    }

    public String flatten(boolean force, boolean ignoreComments, String endEx,
                          boolean blankStrs, boolean blankCmmts,
                          boolean ignoreStrings) {
	if (!force && flattened != null) return flattened;
	Node node = this;
        if (ignoreComments && this.isComment ||
            ignoreStrings && this.isString ||
            endEx != null && Pattern.matches(endEx, end)) return "";
        String text = node.text;
        if (isString && blankStrs) text = text.replaceAll("(.|\n)", ".");
        if (isComment && blankCmmts) text = text.replaceAll("(.|\n)", ".");
	StringBuilder sb = new StringBuilder(node.begin).append(text);
	nodes.sort(new Comparator<Node>() {
	    public int compare(Node a, Node b) { return a.location-b.location;}
	});
	int location = this.location < 0? 0: this.location;
        log.trace("flatten.nodes = " + nodes);
	for (Node n : nodes) {
	    if (n.location < location + sb.length() && n.location >= location)
		sb.insert(n.location-location, 
                          n.flatten(force, ignoreComments, endEx, blankStrs, 
                                    blankCmmts, ignoreStrings));
	    else sb.append(n.flatten(force, ignoreComments, endEx, blankStrs, 
                                     blankCmmts, ignoreStrings));
	}
	flattened = sb.append(node.end).toString();
	return flattened;
    }

    // Try to reconcile nodes which are not true child nodes
    // TODO: convert to Set<Node>. Addall() is tricky, test thoroughly!
    public List<Node> flattenNodes() {
        List<Node> subtree = new ArrayList(nodes); 
	subtree.addAll(outOfBoundsNodes());
	return subtree;
    }

    private Set<Node> outOfBoundsNodes() {
	Set<Node> subtree = new HashSet();
	for (Node n : nodes.toArray(new Node[0])) {
	    if (n.location < location ||
	        n.location > location + getLength()) {
	        subtree.add(n.copy());
	    }
	    if (n.location != location  || n.getLength() != getLength())
                subtree.addAll(n.outOfBoundsNodes());
	}
	return subtree;
    }

    public String getSuperClass(String classDef) {
	boolean extended = false;
        for (String tok: classDef.replaceAll(
	    ".*(class|enum|interface)([\\s\n].*)?$", "").split("[\\s\n]+")) {
            if ("extends".equals(tok)) 
		extended = true;
	    else if (extended) 
		return tok.replaceAll("<.*", "");
        }
        return null; 
    }

    public List<Node> getInnerClasses() {
	if (innerClasses != null) return innerClasses;
	String text = flatten();
	List<Node> classes = new ArrayList();
	List<String> classDefs = getClassDefs(0, getLength(), true);
	log.trace("getInnerClasses.classDefs = "+classDefs + ", this = "+name);
        int i = -1, j = 0;
	while (++i < classDefs.size()) {
            String classDef = classDefs.get(i);
            String def = classDef;
            classDef = classDef.replaceAll(ANNOTATION_REGEX, "");
            if (classDef.contains(")"))
                classDef = classDef.substring(classDef.lastIndexOf(")")+1);
            Node n = getClassDefNode(def);
            log.trace("getInnerClasses.classDef="+classDef+", n=" +n+ ", j="+j+
                     ", this = " + this.name);
	    n.name = getClassName(classDef);
            n.classdef = classDef;
            n.isClass = true;
	    n.pkg = getPackage();
	    n.inherit = n.getClassInheritance(classDef);
	    n.modifiers = n.getClassModifiers(classDef);
            n.classtype = n.getClassType(classDef);
            n.annotations = n.getClassAnnotations(def);
            n.generic = n.getClassGeneric();
            n.fields = n.getFields();
            n.filename = this.filename;
            n.id = null;
            n.colMap();
            log.info("getInnerClasses.classDef = "+classDef + ", n = " + n);
	    // TODO: n.getClassName() will work on n.location is updated
	    classes.add(n);
	}
	log.trace("getInnerClasses.defs = " +classDefs.size()+ ":" + classDefs);
	innerClasses = classes;
	return classes;
    }

    private void updateInnerClass(List<String> members, Node ic, 
            BiFunction<Node, List<String>, List<String>> writefn, 
            Function<Node, List<String>> readfn) {
	for (String ctor: members) {
	    if (readfn.apply(ic) == null) writefn.apply(ic, new ArrayList());
	    if (!readfn.apply(ic).contains(ctor)) readfn.apply(ic).add(ctor);
        }
        if (readfn.apply(this) == null) 
            writefn.apply(this, readfn.apply(ic));
    }

    public static <T> T last(List<T> lst) {
	return last(lst, 0);
    }

    public static <T> T last(List<T> lst, int pos) {
	return lst.size()<(pos+1)? null: lst.get(lst.size()-pos-1);
    }

    public static <T> List<T> concat(List<T>... lists) {
	List<T> lst = new ArrayList();
	for (List<T> a : lists)
	    if (a != null) lst.addAll(a);
	return lst; 
    }

    public static Node merge(Node a, Node b) {
	Node merged = new Node();
	merged.nodes.addAll(a.nodes);
	merged.nodes.addAll(b.nodes);
	return merged;
    }

    public List<Node> split(BiFunction<Node, Node, Boolean> comp) {
        List<Node> splits = new ArrayList();
	splits.add(new Node(this.text));
	for (int i = 0; i < nodes.size()-1; i++) {
	    Node n = nodes.get(i), n1 = nodes.get(i+1);
	    if (comp.apply(n, n1)) splits.add(new Node(this.text));
	    splits.get(splits.size()-1).nodes.add(n);
	}
	return splits;
    }

    public Node findLast(String searchEnd) {
        for (int i = nodes.size()-1; i >=0; i--) {
            if (searchEnd == null || searchEnd.equals(nodes.get(i).end))
                return nodes.get(i);
        }
        return null;
    }

    public Node findByClass(int start, int length) {
        if (members == null) getMembers();
        log.trace("findClass.start="+start+", len="+length+", this=" +this+
            ", classes = " + getMembers().get(2) + 
            ", find = " + find(start, length));
        for (Node node : getMembers().get(2)) 
            if (node.location >= start && 
                node.location + node.getLength() <= start + length) 
                return node.findByClass(start, length);
        log.trace("findClass.return.start="+start+", len="+length+
            ", n="+(isClass ? nodes.get(nodes.size()-1) : this));
        return isClass ? nodes.get(nodes.size()-1).findByClass(start, length) :
            this;
    }

    public Node findTop(String regex) {
        if (isComment) return null;
        log.trace("findTop.regex = " + regex + ", res = " + 
                 Pattern.matches(regex, this.text.replaceAll("\n"," ")) + 
                 ", text = " + this.text);
        if (Pattern.matches(regex, this.text.replaceAll("\n"," ")))
            return this;
        for (Node n : this.nodes)
            if (!Pattern.matches("[\\}\\)]", n.end) && n.findTop(regex)!=null)
                return n;
        log.trace("findTop.regex = " + regex + ", this = " + this.name +
                 ", res = " + null);
        return null;
    }

    public Boolean matchTop(String... regexes) {
        log.trace("matchTop.text = " + flatten().substring(0, 200 > flatten().
            length() ? flatten().length() : 200) + ", n=" + this);
        for (Node n : concat(nodes)) {
            if (!n.isComment && !n.isString && 
                !"}".equals(n.end) && !")".equals(n.end)) {
                if (n.matchTop(regexes)) 
                    return true;
            }
        }
        Node n = this;
        String text = n.begin + n.text + n.end;
        if (!"}".equals(n.end) && !")".equals(n.end) && !n.isComment && 
            !n.isString) {
                for (String regex : regexes) {
                    log.trace("matchTop.reg = " + regex + ", n = " + n + 
                             " -> " + n.flatten());
                    if (Pattern.matches("^(.*\\s)?"+regex+"(\\s.*)?$", 
                            text.replaceAll("\n", " "))) {
                        log.trace("matchTop.reg = "+regex+", match="+text);
                        return true;
                    }
                }
        }
        return false;
    }

 
    public Node findAny(String... toks) {
        for (String tok : toks) {
            Node n = find(tok);
            if (n != null) return n;
        }
        return null;
    }

    public Node find(String tok) {
	return find(tok, null);
    }

    public Node find(String tok, String end) {
	// Searches for string tok, returns deepest, earliest match
	if (isComment) return null;
	for (Node n : nodes) {
	    Node res = n.find(tok, end);
	    if (res!=null) return res;
	}
	return (end==null || end.equals(this.end)) &&
		flatten().contains(tok)? this: null;
    }

    public Node find(int location, int len) {
	if (location < this.location ||
	    location + len > this.location + getLength())
	    return null;
	for (Node n : nodes) {
	    Node res = n.find(location, len);
	    if (res != null) return res;
	}
	return this;
    }

    public Node find(int location) {
	return find(location, null, true); 
    }

    public Node find(int loc, String searchEnd) {
	return find(loc, searchEnd, true);
    }

    public Node findFirst(Function<Node, Boolean> pred) {
        if (pred.apply(this))
	    return this;
	Node res = null;
	for (Node n : nodes) 
	    if ((res = n.findFirst(pred)) != null)
		return res;
	return null;
    }

    public Node find(int location, String searchEnd, boolean includeComments) {
        String end = !isEmpty(this.end)? this.end :
           (flatten().length() <= getLength() || getLength()<1? "":
            flatten().substring(getLength()-1));
        log.trace("find.location = " + location + ", searchEnd = " + searchEnd +
            ", this = " + this.location + ":" + getLength() + ":" + this.end + 
            ":" + this.isComment + ", end = " + end + ", ns = " + nodes);
	boolean outBoundsNode = false;
	for (Node o : outOfBoundsNodes())
	   outBoundsNode |= o.find(location, searchEnd, includeComments) !=null;
	if ((location < this.location ||
	    location > this.location + getLength()) && !outBoundsNode) {
	    return null;
	}
	Node nearest = (includeComments || !this.isComment) &&
	    (searchEnd == null || searchEnd.equals(end))? this: null;
	for (Node n : flattenNodes()) {
	    Node res = n.find(location, searchEnd, includeComments);
	    if (res != null && location - res.location <= 
                               location - (nearest==null? 0: nearest.location))
		nearest = res;
	}
	return nearest;
    }

    public Node findBFS(int location, String searchEnd) {
        List<Node> queue = new ArrayList();
        queue.add(this);
	while (!queue.isEmpty()) {
            Node n = queue.get(0);
            queue = queue.subList(1, queue.size());
	    if (location > n.location + getLength()) 
	        continue;
	    if (searchEnd==null || searchEnd.equals(n.end))
                return n;
            queue.addAll(n.flattenNodes());
	}
	return null;
    }


    public int resetLength() {
        flattened = null;
	length = begin.length() + text.length() + end.length();
	for (Node n : nodes) 
	    length += n.resetLength();
	return length;
    }

    public int getLength() {
	if (length < 0) {
	    length = flatten().length();
	}	
	return length;
    }
  
    public int endLocation() {
        return location + getLength();
    }

    public Node setTemplate(Datum template) {
	put("template_id", template.get("id"));
	put("template_name", template.get("name"));
	return this;
    }

    public Node setFile(String fname) {
        if (fname != null) {
            put("filename", fname);
            if (get("name")==null || "".equals(get("name")))
                put("name", fname.replaceAll("(.*[\\/\\\\]|\\..*)", ""));
            put("text", text);
        }
        return this;
    }

    public Node setInst(String inst) {
	appInstance = inst;
	return this;
    }

    public Node setOwner(String user) {
	owner = user;
	return this;
    }

    @Override
    public Datum newInstance(Map<String, Object>... data) {
	Datum n = new Node();
        for (Map<String, Object> m : data)
            n = n.merge(m);
        return n;
    }

    //@Override
    public Object get(String col) {
	Object val = super.get(col);
	if (val == null && containsKey("properties"))
	    val = ((HashMap) super.get("properties")).get(col);
	if (val == null && containsKey("listproperties"))
	    val = ((HashMap) super.get("listproperties")).get(col);
	return val; 
    }

    @Override
    public Object put(String key, Object value) {
	List<String> dbCols = Arrays.asList("id", "name", "inputs", "outputs",
	    "resources", "operations", "edges","template_id","owner","node_id",
            "package", "servers");
	if (value == null) {
	    if (containsKey(key)) return remove(key);
	    if (containsKey("properties")) 
		((HashMap) get("properties")).remove(key);
	    if (containsKey("listproperties")) 
		((HashMap) get("listproperties")).remove(key);
	    return value;
	}
        if (dbCols.contains(key)) 
	    return super.put(key, "template_id".equals(key)? str(value): value);
	if (Arrays.asList("properties", "listproperties").contains(key)) {
	    if (get(key) == null) return super.put(key, value);
	    else ((Map)get(key)).putAll((Map) value);
	    return value;
	}
	String props = value instanceof Collection? "listproperties":
	    "properties";
	if (!containsKey(props))
	    super.put(props, new HashMap());
	return ((HashMap) get(props)).put(key, value);
    }

    private String str(Object val) {
        return val == null ? null : val.toString();
    }

    public boolean containsKey(String k) {
        if (super.containsKey(k)) return true;
        if (super.get("properties") != null && 
            ((Map)super.get("properties")).containsKey(k))
            return true;
        return (super.get("listproperties") != null && 
            ((Map)super.get("listproperties")).containsKey(k));
    }


    //@Override
    public Set<String> keySet1() {
        Set<String> ks = new HashSet();
        ks.addAll(super.keySet());
	if (containsKey("properties"))
	    ks.addAll(((HashMap) get("properties")).keySet());
	if (containsKey("listproperties"))
	    ks.addAll(((HashMap) get("listproperties")).keySet());
	return ks; 
    }

    private void putNonEmpty(String key, Object... vals) {
	for (Object val : vals) {
	    if (!isEmpty(val)) {
		this.put(key, val);
		return;
	    }
	    if (!containsKey(key)) 
		this.put(key, val);
	}
    }

    public static Datum toIndexDoc(Node n) {
	Datum d = new Node();
	for (String k : Arrays.asList("id", "name", "text", "template_id"))
	    d.put(k, n.get(k));
	if (n.get("properties") != null) {
	    for (String k : ((Map<String,String>)n.get("properties")).keySet()) 
	        if (Pattern.matches("(behaviour|template_name|package).*", k))
		    d.put(k + "_s", ((Map) n.get("properties")).get(k));
	}
	return d;
    }

    @Override
    public Map<String, Object> colMap() {
	putNonEmpty("name", this.name, getClassName(), "");
        putNonEmpty("filename", this.filename);
	putNonEmpty("text", this.text);
	//putNonEmpty("score", this.score);
	putNonEmpty("template_id",
            this.templateid == null? null : this.templateid.toString());
	//this.put("inputs", this.inputs);
	//this.put("outputs", this.outputs);
	putNonEmpty("appinstance", this.appInstance);
	putNonEmpty("owner", this.owner);
	putNonEmpty("package", this.getPackage());
	putNonEmpty("servers", this.servers);
	List kwords = new ArrayList();
	for (String k : Arrays.asList("properties", "listproperties"))
	    this.put(k, get(k)==null? new HashMap(): get(k));
	this.put("keywords", kwords);
        if (this.id == null && get("id") == null) {
             this.id = toUUID(""+("".equals(get("filename")) ? get("package") : 
                           get("filename"))+"."+this.get("name")).toString();
        } else if (this.id == null) this.id = get("id").toString();
	this.put("id", this.id);
        log.trace("colMap() = " + (Map) this);
        return this;
    }

    public Node setLocation() {
	int loc = location<0? 0: location, i = -1;
	while (++i < nodes.size()) {
	    Node n = nodes.get(i);
	    n.setLocation();
	    loc += n.isComment? 0: (n.begin + n.text + n.end).length();
	}
	return this;
    }

    public void reorder() {
	Function<Node, Predicate<Node>> filter = p -> { 
	    return n -> n.location >= p.location && 
	        n.location < p.location + p.getLength(); };
	List<Node> rnodes = new ArrayList();
	for (Node n : nodes) {
	    rnodes.addAll(n.nodes); n.nodes.clear();
	}
        for (Node n : nodes) {
	    n.nodes.addAll(rnodes.stream().filter(filter.apply(n)).
	        collect(Collectors.toList()));
	}
	log.trace("reorder.nodes = " + rnodes.size());
    }

    public List<Node> asParameters() {
	List<Node> ns = new ArrayList();
	ns.addAll(nodes);
	if (nodes.size()>1 && !nodes.get(0).end.equals(",") ||
	    nodes.isEmpty() && text.length() > 0) {
	    Node n = copy();
	    ns.clear(); ns.add(n); 
	    n.begin = ""; n.end = "";
	}
	return ns;
    }

    public boolean isInProjPath(String entname) {
	Node n = this;
	boolean inPath = 
	    getMatches(CLASS_REGEX).stream().filter(c -> Pattern.matches(
	    "(.|\n)*(class|enum|interface)[\\s\n]+" + entname +
	    "[\\s\n](.|\n)*", c)).findFirst().isPresent() ||
	    n.filename != null && (Files.exists(Paths.get(
	    n.filename.replaceAll("[^\\/\\\\]*.java$", entname+".java"))) ||
	    exists(paths(n.filename).stream().map(
		p -> p + entname + ".java")));
	log.trace("isInProjPath entname = " + entname + ", " + inPath);
	return inPath;
    }

    public boolean isInJavaClassPath(String entname) {
	Node n = this;
	if (entname.trim().length() <= 1) return false;
	if (n.filename == null || !Files.exists(Paths.get(
	    n.filename.replaceAll("[^\\/\\\\]*.java$", entname+".java")))) {
	    log.trace("isInJavaClassPath paths = " + paths(n.filename));
	    if (!exists(paths(n.filename).stream().map(
		p -> p + entname + ".java"))) {
		if (toClass("java.lang."+entname, 
		    getImports().stream().filter( i ->
			Pattern.matches(".*java.util.(\\*|"+entname+").*", i)).
			findAny().isPresent()? "java.util."+entname: entname,
		    entname) != null) {
		    log.trace("isInJavaClassPath true = " + entname);
		    return true;
		}
	    }
	}
	log.trace("isInJavaClassPath false = " + entname);
	return false;
    }

    public static boolean exists(Stream<String> pathStream) {
	return pathStream.filter(path -> Files.exists(Paths.get(path))).
		findFirst().isPresent();
    }

    public Class toClass(String... altNames) {
	Class ref = null;
	List<String> names = new ArrayList();
	for (String name : altNames) {
	    names.add(name);
	    if (Pattern.matches("^[A-Za-z0-9_]+$", name)) {
	        names.remove(names.size()-1);
		for (String imp: getImports()) 
		    if (imp.startsWith("java") && (imp.contains(name) ||
			imp.endsWith("*"))) {
			names.add(imp.contains(name)? imp: 
			    imp.replaceAll("\\*", "") + name);
		    }
	    }
	}
	for (String name : names) {
	    try { 
		ref = Class.forName(name);
		if (ref != null) return ref;
	    } catch (ClassNotFoundException e) { }
	}
	log.trace("toClass.ref = " + ref + ", names = " + names);
	return ref;
    }

    public List<String> paths(String path) {
	if (pathMap.containsKey(path)) return pathMap.get(path);
	List<String> pkgs = new ArrayList();
	if (path == null) return pkgs;
	String pkg = "", delim = ".";
	List<String> canonicals = Arrays.asList("main", "java");
	Matcher m = Pattern.compile("([\\.\\/\\\\])").matcher(path);
	if (m.find()) delim = m.group();
	for (String tok : path.split("[\\.\\/\\\\]")) {
	    if (!pkgs.isEmpty() && pkgs.get(pkgs.size()-1).
		endsWith("src" + delim) && !canonicals.contains(tok)) {
		pkgs.addAll(importPaths(pkg + canonicals.stream().
		    collect(Collectors.joining(delim)), delim));
		pkgs.addAll(importPaths(pkg + canonicals.get(
		    canonicals.size()-1), delim));
	    }
	    pkg += tok + delim;
	    pkgs.add(pkg);
	}
	pathMap.put(path, pkgs);
	return pkgs;
    }

    public List<String> importPaths(String base, String delim) {
	List<String> paths = new ArrayList();
	if (!Files.exists(Paths.get(base))) return paths;
	for (String imp : concat(getImports(), Arrays.asList(getPackage()+".")))
	    if (!imp.trim().startsWith("java")) 
		paths.add(base + delim + Arrays.asList(
		    imp.replaceAll("\\*", "").
		    replaceAll("[^\\.]+$", "").split("\\.")).
		    stream().collect(Collectors.joining(delim)) + delim);
	return paths;
    }

    public String lastText() {
        return texts.isEmpty()? "": texts.get(texts.size()-1);
    }

    public Node root() {
        Node root = this;
	while (root.parent != null)
	    root = root.parent;
	return root;
    }

    public Node wrap() {
	if (parent != null) {
            parent.wrapText();
	    //parent.textNodes.add(this);
	    return parent;
	} 
	return this;
    }

    public List<String> wrapText() {
	int prev = String.join("", texts).length();
        List<String> splits = null;
	if (texts.isEmpty()) {
            splits = splitText();
            if (splits.isEmpty()) texts.add(this.text);
            else texts.addAll(splits);
        } else {
	    texts.add(prev >= text.length()? "" : text.substring(prev));
            splits = splitText();
        }
        log.trace("wrapText.texts = " + texts + ", this = " + this + 
                  ", splits = " + splits);
        return texts;
    }

    // Splits the text field based on child nodes. Similar code to flatten()
    public List<String> splitText() {
        List<String> splits = new ArrayList();
        Node node = this;
	StringBuilder sb = new StringBuilder(node.begin);
        if (texts.isEmpty()) sb.append(node.text);
        else for (String txt : texts) sb.append(txt);
	nodes.sort(new Comparator<Node>() {
	    public int compare(Node a, Node b) { return a.location-b.location;}
	});
	int location = this.location < 0? 0: this.location;
        int curr = node.begin.length(); textNodes.clear();
        boolean force = false, ignoreComments = false;
        textNodes.add(new ArrayList());
        String tail = "";
	for (Node n : nodes) {
            textNodes.get(textNodes.size()-1).add(n);
	    if (n.location < location + sb.length() && n.location >= location) {
                splits.add(curr >= n.location - location ? "" :
                    sb.toString().substring(curr, n.location-location));
                n.flattened = null;
		sb.insert(n.location-location, n.flatten(force,ignoreComments));
                textNodes.add(new ArrayList());
                curr = n.location - location + n.getLength();
                if (curr < sb.length()) tail = sb.toString().substring(curr);
            } else sb.append(n.flatten(force, ignoreComments));
	}
	if (tail.length() > 0) {
            splits.add(tail);
            textNodes.add(new ArrayList());
        }
        log.trace("splitText.textNodes = "+toString(textNodes.toArray(
            new ArrayList[0]))+", ns = "+toString(nodes) +", splits="+splits);
	return splits;
    }

    public static String defaultValue(String type) {
	return Pattern.matches("(.*\\s+)?(byte|short|int)(\\s+.*)?", type)? "0":
	   (type.contains("long")? "0L": 
	   (type.contains("char")? "'0'": 
	   (type.contains("void")? "": 
	   (type.contains("boolean")? "false": "null"))));
    }

    public static Node parseDef(String def) {
        Node n = new Node();
	def = def == null? def : def.trim();
	if (Pattern.matches("(" + CTOR_REGEX + "|" + METHOD_REGEX + "|" + 
                                  CALL_REGEX + ")", def)) {
	    String[] toks = def.trim().split("[\\(\\)]");
	    n.qualifiers = new ArrayList(Arrays.asList(
                toks[0].split("[\\s\n]+")));
	    log.trace("parseDef.qualifiers = " + n.qualifiers);
	    n.name = n.qualifiers.get(n.qualifiers.size()-1);
	    n.qualifiers.remove(n.qualifiers.size()-1);
            if (!n.qualifiers.isEmpty()) {
	        n.retval = n.qualifiers.get(n.qualifiers.size()-1);
	        n.qualifiers.remove(n.qualifiers.size()-1);
            }
	    if (toks[1].trim().length() > 0)
	      for (String param : splitTopLevel(toks[1], ",")) {
	        Node p = parseDef(param + " () {");
	        n.nodes.add(p);
	      }
	    n.put("name", n.name);
	    n.put("qualifiers", n.qualifiers);
	    n.put("retval", n.retval);
	}
	return n;
    }

    public String getComments() {
        StringBuilder sb = new StringBuilder();
        if (isComment) 
            sb.append("<comment>").append(flatten()).append("</comment");
        else 
            for (Node n : nodes)  
                sb.append(n.getComments());
        return sb.toString();
    }

    public static List<Node> sort(List<Node> nodes) {
	nodes.sort(new Comparator<Node>() {
	    public int compare(Node a, Node b) { return a.location-b.location;}
	});
        return nodes;
    }

    public List<List<Node>> aggregate() {
        return aggregate(true);
    }

    public List<List<Node>> aggregate(boolean avoidTop) {
        return aggregate(avoidTop, false);
    }

    public List<List<Node>> aggregate(boolean avoidTop, boolean force) {
        if (aggregates != null && !force) return aggregates;
        if (avoidTop && location <= 0) {
            Node n = findBFS(0, "}");
            log.trace("aggregate.avoidtop.n = " + n);
            if (n != null && !n.equals(this)) return n.aggregate();
        }
        List<List<Node>> aggs = new ArrayList();
        Node prev = null;
        for (Node n : memberNodes()) {
            if (prev != null && chained(prev, n)) {
                addInPlace(prev, n);
                prev.length += n.getLength(); prev.flattened = null;
            } else {
                if (aggs.isEmpty()) aggs.add(new ArrayList());
                aggs.get(aggs.size()-1).add(n);
                if (Pattern.matches("[\\};]", n.end) || !n.isString && 
                    Pattern.matches("[\\};]", n.flatten().trim()) ||
                    "enum".equals(getClassType()) && !Arrays.asList(
                        n.flatten().split("\\s")).contains("throws") &&
                        (",".equals(n.end) ||
                         ",".equals(n.prev().end) && n.next() == null)) 
                    aggs.add(new ArrayList());
                prev = n;
            }
        }
        log.trace("aggregates.aggs = " + aggs.stream().map(l -> "agg = " + 
            new Node(l).flatten()).collect(Collectors.toList()) + 
            ", classType = " + getClassType());
        if (!force) aggregates = aggs;
        return aggs;
    }

    private List<Node> memberNodes() {
        List<Node> ns = new ArrayList();
        Node node = this;
        log.trace("memberNodes.nodes = " + node.nodes);
        for (Node n : sort(node.nodes)) {
            if (n.isComment) 
                continue;
            if (isEmpty(n.end) && isEmpty(n.begin) && isEmpty(n.text) && 
                !n.nodes.isEmpty())
                ns.addAll(n.memberNodes());
            else ns.add(n); 
        }
        log.trace("memberNodes().ns = " + ns + ", strs = " + 
                  ns.stream().map(n -> n.flatten()).
            collect(Collectors.toList()));
        return ns;
    }

    private static boolean isEmpty(String s) {
        return s == null || "".equals(s);
    }

    public List<Node> topLevel(List<Node> ns) {
        List<Node> tops = new ArrayList();
        for (Node n : ns) {
            log.info("topLevel().n = " + n);
            if (!Pattern.matches("[\\}]", n.end) && n.findLast(";") != null) {
                 tops.addAll(n.nodes);
                 log.info("topLevel().nodes = " + n.nodes + ", n = " + n);
            }
            tops.add(n);
        }
        return tops;
    }

    public List<String> getClassMembers() {
        if (classmembers != null) return classmembers;
        Node node = find(location, getLength()); 
        node = (node == null ? this : node);
        List<String> txts = groupClassMemberTexts(node.aggregate(true, true).
            stream().filter(l -> !l.isEmpty()).
            map(l -> l.stream().filter(n -> !n.isComment).
                       collect(Collectors.toList())).
            map(l -> new Node(isField(new Node(l)) ||
               l.isEmpty() || !"}".equals(l.get(l.size()-1).end) ? l :
               l.subList(0,l.size()-1))).
            collect(Collectors.toList()));
        if (classdef == null && this.parent != null && 
            (classmembers == null || classmembers.isEmpty())) 
            classmembers = this.parent.getClassMembers();
        log.trace("getClassMembers().name = " + name + ", txts = " + txts);
        classmembers = txts;
        return classmembers;
    }

    public List<List<Node>> getMembers() {
        if (members != null && !members.isEmpty()) return members;
        log.trace("getMembers().this = " + this);
        List<Node> fields = new ArrayList();
        List<Node> methods = new ArrayList();
        List<Node> classes = new ArrayList();
        List<Node> blocks = new ArrayList();
        for (Node n : nodes) {
            if (n.isComment) continue;
            if (Pattern.matches("[\\)\\}\\>\\\"']", n.end))
                blocks.add(n);
            else if ("enum".equals(classtype) && (",".equals(n.end) ||
                ",".equals(n.prev().end) && n.next() == null) || 
                     ";".equals(n.end))
                fields.add(n);
            else if (Pattern.matches(CLASS_REGEX + ".*", 
                     n.flatten().replaceAll("[\n]", " ")))
                classes.add(n);
            else
                methods.add(n);
        }
        log.trace("getMembers.b4.fields = " + toString(fields) + 
            ", methods = " + toString(methods) + ", blocks = "+toString(blocks)+
            ", classes = "+ toString(classes) + ", nodes = " + toString(nodes));
        move(fields, methods, classes, blocks);
        List<Node> rest = methods.stream().filter(
            m -> !m.isMethod && !m.hasBraces).collect(Collectors.toList());
        methods = methods.stream().filter(m -> m.isMethod).collect(Collectors.
            toList());
        move(fields, Collections.emptyList(), Collections.emptyList(), rest);
        members = Arrays.asList(fields, methods, classes, rest);
        log.trace("getMembers.blocks = " + toString(blocks));
        log.trace("getMembers.fields,methods,classes,rest = " + 
            toString(fields, methods, classes, rest)); 
        return members;
    }

    public static boolean chained(Node l, Node r) {
        int end = l.location + l.getLength();
        return r.location >= l.location && r.location < end;
    }

    public static boolean overlapped(Node l, Node r) {
        int end = l.location + l.getLength();
        return r.location >= l.location && r.location <= end;
    }

    public static boolean spans(Node l, Node r) {
        int end = l.location + l.getLength()-1;
        return r.location >= l.location && r.location <= end;
    }


    public void move(List<Node> fields, List<Node> methods, List<Node> classes, 
        List<Node> blocks) {
	blocks.sort(new Comparator<Node>() {
	    public int compare(Node a, Node b) { return a.location-b.location;}
	});
        List<Node> blocksRev = new ArrayList(blocks); 
        Collections.reverse(blocksRev);
        Node prev = null;
        List<Node> rm = new ArrayList();
        for (Node m : Node.concat(fields, methods, classes)) {
            for (Node b : Node.concat(blocks, blocksRev)) {
               if ((chained(m, b) || overlapped(b, m) && !"}".equals(b.end)) && 
                   addInPlace(m,b)>=0){
                   m.length += b.getLength(); m.flattened = null;
                   rm.add(b);
                   log.trace("move.b = " + b + ", m = " + m + ", i=" + 
                       blocks.indexOf(b) + ", this = " + this);
                   if (")".equals(b.end)) m.hasBraces = true;
                   if ("}".equals(b.end) && classes.contains(m))
                       m.isClass = true;
                   else if ("}".equals(b.end)) m.hasCurly = true;
                   if (m.hasBraces && m.hasCurly) m.isMethod = true;
               }
            }
            remove(blocks, rm);
            prev = m;
        }
        remove(this.nodes, rm);
    }

    public static int addInPlace(Node target, Node n) {
        List<Node> sorted = target.nodes;
        log.trace("addInPlace.target = " + target + ", n = " + n);
        for (Node c : sorted) 
            if (c.location == n.location && c.getLength()==n.getLength())
                return -1;
        if (n.location < target.location) 
            return addLeft(target, n);
        //else if (n.location >= target.location + target.getLength()) 
        else if (!Pattern.matches("[\\)\\}\\>\\\"']", n.end))
            return -1;
        for (int i = 0; i < sorted.size(); i++) {
            if (sorted.get(i).location > n.location) {
                sorted.add(i, n);
                return i;
            }
        }
        sorted.add(n);
        return sorted.size()-1;
    }

    public static int addLeft(Node target, Node n) {
        List<Node> sorted = target.nodes;
        if ((target.begin + target.text).length() > 0) {
            sorted.add(0, new Node(target.text, n.location+n.getLength()));
            sorted.get(0).begin = target.begin;
            sorted.get(0).parent = target;
            target.begin = ""; target.text = "";
        }
        sorted.add(0, n);
        target.location = n.location;
        return 0;
    }

    public static void remove(List<Node> nodes, List<Node> rm) {
        List<Node> copy = new ArrayList();
        for (Node n : nodes) {
            boolean found = false;
            for (Node r : rm)
                if (r.location==n.location && r.getLength()==n.getLength())
                    found = true;
            if (!found) copy.add(n);
        }
        nodes.clear();
        nodes.addAll(copy);
    }

    public int index() {
        if (parent == null) return 0;
        int i = 0;
        for (Node n : parent.nodes) 
            if (n.location < location)
                i++;
        return i;
    }

    public Node next() {
        if (parent == null) return null;
        Node nxt = null;
        for (Node n : parent.nodes) 
            if (n.location > location && (nxt == null ||
                n.location - location < nxt.location - location))
                nxt = n;
        log.trace("next().this=" +this+ ", nxt=" + nxt + ", parent=" + parent);
        return nxt;
    }

    public Node prev() {
        if (parent == null) return null;
        Node p = null;
        for (Node n : parent.nodes) 
            if (n.location < location && (p == null ||
                location - n.location < location - p.location))
                p = n;
        return p == null ? parent : p;
    }

    public boolean equals(Node o) {
        return o!=null && location==o.location && getLength()==o.getLength();
    }

    // Performs a shallow copy
    public Node copy() {
	Node cp = new Node();
	cp.text = text; cp.begin = begin; cp.end = end; cp.name = name; 
	cp.score = score; //cp.inputs = inputs; cp.outputs = outputs; 
	cp.templateid = templateid; cp.id = id; cp.level = level; 
	cp.location = location; cp.length = length; 
	cp.nodes = new ArrayList(nodes);
	cp.parent = parent; 
	cp.compiles = compiles; cp.runs = runs; cp.isComment = isComment;
	cp.hasMain = hasMain; cp.filename = filename; 
	cp.methods = methods==null? null: new ArrayList(methods);
	cp.ctors = ctors==null? new ArrayList(): new ArrayList(ctors);
	cp.appInstance = appInstance; cp.owner = owner;
	return cp;
    }
    

    public static String toString(List<Node>... nsParams) {
        StringBuilder sb = new StringBuilder();
        for (List<Node> ns : nsParams)
            sb.append(sb.length() == 0 ? "" : ", ").
               append("[").append(ns.size()).append(": ").append(
                ns.stream().map(n -> n.location + ":" + n.getLength()).
                collect(Collectors.toList())).append("]");
        return sb.toString();
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder("{ text = ");
	sb.append(begin).append(":").append(
	    text.length()<200? text: text.substring(0, 200)).
		append(":").append(end).
            append(", name = ").append(name). 
	    append(", filename = ").append(filename).
	    append(", location = ").append(location).
	    append(", length = ").append(getLength()).
	    append(", isComment = ").append(isComment).
	    append(", isString = ").append(isString).
	    append(", hasBraces = ").append(hasBraces).
	    append(", hasCurly = ").append(hasCurly).
	    append(", isClass = ").append(isClass).
	    append(", isMethod = ").append(isMethod).
	    append(", texts = ").append(texts.size()).
	    append(", qualifiers = ").append(qualifiers).
	    append(", generic = ").append(generic).
	    append(", modifiers = ").append(modifiers).
	    append(", retval = ").append(retval).
	    append(", inheritance = ").append(inherit).
	    append(", classdef = ").append(classdef).
	    append(", classtype = ").append(classtype).
	    append(", annotations = ").append(annotations).
	    append(", methods = ").append(methods).
	    append(", ctors = ").append(ctors).
	    append(", fields = ").append(fields).
	    append(", servers = ").append(servers).
	    append(", nodes = ").append(toString(nodes));
	int i = 0;
        return sb.append("] ").append(super.toString()).append("}").toString();
    }
}
