package com.eonforge.delphi.db.model;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import com.eonforge.delphi.db.Datum;

public class Searchable extends Datum {

    public String keyspace = null;

    public Searchable(String keyspace) { this.keyspace = keyspace; }

    @Override
    public String table() { return keyspace; }

    @Override 
    public Map<String, Object> colMap() {
	for (String k : Arrays.asList("id", "name", "content", "title", "owner",
				"amount_d", "time_l"))
	   if (!containsKey(k)) {
		if (k.endsWith("_d")) put(k, 0.0d);
		else if (k.endsWith("_i")) put(k, 0);
		else if (k.endsWith("_l")) put(k, 0l);
		else put(k, "");
	   }
	return this;
    }


    @Override
    public Datum newInstance(Map<String, Object>... data) { 
	Searchable doc = new Searchable(keyspace);
	for (Map<String, Object> datum : data)
	    doc.merge(datum);
	return doc;
    }

}
