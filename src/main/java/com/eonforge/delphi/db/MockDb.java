package com.eonforge.delphi.db;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.eonforge.peregrine.Config;
import com.eonforge.peregrine.parse.Json;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockDb<T extends Datum> {

    private T seed;
    private String resource;
    private List<T> data = new ArrayList();
    public static Boolean isEmbedStarted = false;
    private static final Logger log = LoggerFactory.getLogger(MockDb.class);

    public MockDb(T seed) { 
	this.seed = seed; 
	this.resource = seed.table().replaceAll("kbase.", "") + ".json"; 
	load();
    }
	
    public static void initEmbedded(String host, String port) {
	log.info("initEmbedded.isEmbedStarted = " + isEmbedStarted);
	if (isEmbedStarted) return;
	String yml = getContent("cassandra-embed.yml");
	log.info("initEmbedded.host,port = " + host + ", " + port);
	if (!yml.contains("native_transport_port: " + port) ||
	    !yml.contains("listen_address: " + host))
	    return;
	log.info("Starting embedded cassandra");
	try {
            System.setProperty("cassandra.config", 
                MockDb.class.getResource("/cassandra-embed.yml").toString());
	    EmbeddedCassandraServerHelper.startEmbeddedCassandra(
	        "cassandra-embed.yml", Long.valueOf(
	        Config.getProperty("cassandra-startup-duration", "300000")));
	} catch (Exception e) { log.error(e.getMessage(), e); return; }
	log.info("Initializing embedded cassandra");
	Cluster cluster = Cluster.builder().addContactPoints(host).withPort(
	    Integer.valueOf(port)).build();
	Session session = cluster.connect();
	for (String stmt : getContent("delphi.cql").split(";")) {
            log.info("running " + stmt);
	    if (stmt.trim().length()>0) session.execute(stmt + ";");
	}
	isEmbedStarted = true;
    }


    // Accepts resource string, i.e. resource="file/test.json"
    public void load() {
	data.addAll(getData());
    }

    private static Scanner getScanner(String resource) throws IOException {
	try {
	    if (!resource.startsWith("/")) resource = "/" + resource;
	    return new Scanner(MockDb.class.getResourceAsStream(resource));
	} catch (Exception e) {
	    log.error("Unable to get resource stream: " + e.getMessage(), e);
	    return new Scanner(new File(resource));
	}
    }
	
    public static String getContent(String resource) {
	StringBuilder sb = new StringBuilder();
	try (Scanner scanner = getScanner(resource)) {
            while (scanner.hasNextLine())
		sb.append(scanner.nextLine()).append("\n");
	    scanner.close();
	} catch (IOException e) { log.error("Error loading " + resource, e); }
	return sb.toString();
    }

    public List<T> getData() {
	return getData(this.resource);
    }

    public List<T> getData(String resource) {
	if (MockDb.class.getResource(resource.startsWith("/")? resource:
	    "/" + resource) == null)
	    return Collections.emptyList();
	Map map = (Map) fromJson(parse(getContent(resource)));
	log.trace("getData.fromJson = " + map);
	log.trace("getData.outputs = " + map.get("output_0"));
	List<T> data = 
	    (List) ((List) ((Map) fromJson(parse(getContent(resource)))).
	    get("docs")).stream().map(d -> seed.newInstance().merge((Map) d)).
	        collect(Collectors.toList());
	log.trace("getData.data = " + data);
	return data;
    }

    List<T> search(Map... queries) {
	List<T> rows = new ArrayList();
	if (queries.length == 0) rows.addAll(data);
	else {
	    for (T row : data) 
		for (Map qry : queries)
	            for (Object key : qry.keySet())
		        if (row.get(String.valueOf(key)) != null && 
		            row.get(String.valueOf(key)).equals(qry.get(key)))
		            rows.add(row);
	}
	return rows;
    }

    boolean addAll(List<T> data) {
	try {
	    this.data.addAll(data);
	} catch (Exception e) { return false; }
	return true;
    }

    boolean update(T d) {
	try {
	    search((Map) d).get(0).merge(d);
	    return true;
	} catch (Exception e) { log.error("Error updating " + d, e); }
	return false;
    }

    boolean delete(T d) {
	try {
	    data.remove(search((Map) d));
	    return true;
	} catch (Exception e) { log.error("Error deleting " + d, e); }
	return false;
    }


    public static Object parse(String json) {
	return Json.parse(json);
    }

    public static Object fromJson(Object json) {
	return Json.fromJson(json);
    }
}
