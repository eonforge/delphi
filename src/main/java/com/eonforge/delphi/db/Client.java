package com.eonforge.delphi.db;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import com.eonforge.peregrine.Config;

import com.datastax.driver.core.*;
import com.datastax.driver.core.exceptions.*;
import com.google.common.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Client<T extends Datum> {

    private Cluster cluster = null;
    private Session session = null;
    private T type = null;
    private int port = -1;
    private String host = null;
    private String keyspace = null;
    private static Map<String, Client> instMap = new ConcurrentHashMap();
    private static final Logger log = LoggerFactory.getLogger(Client.class);
    public static int readTimeoutMs = 300, connectTimeoutMs = 300;
    public static int initConnsPerHost = 1, maxConnsPerHost = 1;
    public static int maxInflightQueries = 1;
    public static final int MAX_RETRIES = 3;
    public static final int RETRY_SECS = 5;


    private Client(String host, String port, String keyspace) {
	int portInt = port == null? 9342: Integer.valueOf(port);
	host = host==null? "127.0.0.1": host;
        this.port = portInt; 
        this.host = host;
        this.keyspace = keyspace;
	if (Config.getPropertyInt("dbinstances", 0) <= 0)
	    MockDb.initEmbedded(host, String.valueOf(portInt));
	log.info("Building host = "+host+", port="+portInt+", keyspace=" +
	    keyspace + ", dbinstances = " + Config.getProperty("dbinstances"));
        loadSession();
    }

    public static void loadConfig(String propfile) {
        Config.load(propfile);
        log.info("loaded config from " + propfile + ", dbinstances = " +
                 Config.getPropertyInt("dbinstances", 0));
    }

    public boolean loadSession() {
        return withRetries(i -> {
            cluster = Cluster.builder().withPort(port).addContactPoint(host).
		withSocketOptions(getSocketOptions()).
                withPoolingOptions(getPoolOptions()).build();
	    log.trace("Cluster = {}", cluster);
	    session = cluster.connect(keyspace.replaceAll("\\..*", ""));
	    log.trace("session = {}", session);
            return true;
        });
    }

    private static SocketOptions getSocketOptions() {
        return new SocketOptions().setReadTimeoutMillis(readTimeoutMs).
                                   setConnectTimeoutMillis(connectTimeoutMs);
    }

    private static PoolingOptions getPoolOptions() {
        return new PoolingOptions().setConnectionsPerHost(HostDistance.REMOTE, 
            initConnsPerHost, maxConnsPerHost);
    }

    public static synchronized Client instance(String host, String port, 
	String db) {
	String key = host + ":" + port + ":" + db;
	log.trace("Client.instance.key = " + key);
	if (!instMap.containsKey(key))
	    instMap.put(key, new Client(host, port, db));
	return instMap.get(key);
    }

    public static synchronized Client instance(Datum type) {
	Client inst = instance(type.table());
	inst.type = type;
	if (MockDb.isEmbedStarted) 
	    for (Object d : new MockDb(type).getData())
		inst.insert((Datum) d, false);
	return inst;
    }

    public static synchronized Client freeInstance(Datum type) {
        return new Client(Config.getProperty("dbhost"),
			  Config.getProperty("dbport"), type.table());
    }

    public static synchronized Client instance(String keyspace) {
	for (String key : instMap.keySet())
	    if (key.endsWith(":" + keyspace))
		 return instMap.get(key);
        return instance(Config.getProperty("dbhost"),
			Config.getProperty("dbport"), keyspace);
    }

    public int getInFlightQueries() {
        return session.getState().getInFlightQueries(
            session.getState().getConnectedHosts().iterator().next());
    }

    public Client test(String table) {
	type = (T) new Datum() {
	    public String table() { return "node"; }
	    public Map<String, Object> colMap() {
		Map<String, Object> m = new HashMap(this);
		m.put("name", "testname");
		m.put("properties", new HashSet(Arrays.asList("prop1")));
		m.put("operations", new HashSet(Arrays.asList("op1")));
		m.put("edges", new HashSet(Arrays.asList("edge1")));
		return m;
	    }
            public Datum newInstance(Map<String, Object>... m) { return this; }
	};
	insert(type);
	for (Datum row : fetchAll()) {
	//	Row row : session.execute("SELECT * FROM " + table)) {
	    log.info("row = " + row);
	}
	delete(type);
	log.info("test done");
	return this;
    }

    public boolean insert(Datum r) {
	return insert(r, true);
    }

    public boolean insert(Datum r, boolean resetId) {
        StringBuilder query = new StringBuilder("INSERT INTO ");
	query.append(r.table()).append(" (");
	StringBuilder vals = new StringBuilder(") VALUES (");
	if (resetId) r.put("id", UUID.randomUUID());
	int i = 0, ilen = query.length(), vlen = vals.length();
	log.trace("insert.r = " + r);
	for (String col : r.columns()) {
	    query.append(query.length()==ilen? "": ", ").append(col);
	    vals.append(vals.length()==vlen? "": ", ").append(r.valStr(col));
	}
	query.append(vals.toString()).append(")");
	log.info("Running query " + query.toString());
        return withRetries(n -> session.execute(query.toString()).wasApplied());
    }

    public boolean withRetries(Function<Integer,Boolean> fn) {
        for (int i = 0; i < MAX_RETRIES; i++) {
            try {
                return fn.apply(i);
            } catch (UnavailableException|WriteTimeoutException e) {
                log.error("Failure, retry waiting " + RETRY_SECS + "s", e);
                try {
                    Thread.sleep(RETRY_SECS*1000);
                } catch (InterruptedException ie) { }
            }
        }
        return false;
    }

    public boolean update(Datum r) {
        StringBuilder query = new StringBuilder("UPDATE ");
	query.append(r.table()).append(" SET ");
	log.trace("update.r = " + r);
	String start = query.toString();
	for (String col : r.columns()) {
	    if (!"id".equals(col)) {
		if (r.get(col) instanceof Map) 
		    for (Object k : ((Map) r.get(col)).keySet())
                        query.append(start.equals(query.toString()) ? "": ", ").
	                    append(col).append("['").append(k).append("']=").
			    append("'" + ((Map) r.get(col)).get(k) + "'");
		else 
                    query.append(start.equals(query.toString()) ? "": ", ").
	                append(col).append(" = ").append(r.valStr(col));
	    }
	}
	query.append(" WHERE id = ").append(r.valStr("id"));
	log.trace("Running query " + query.toString());
        return withRetries(n -> session.execute(query.toString()).wasApplied());
    }

    public boolean delete(Datum r) {
	String query = "DELETE FROM " + r.table() + " WHERE id="+r.get("id");
	log.trace("Executing " + query);
        return withRetries(n -> session.execute(query.toString()).wasApplied());
    }


    public List<T> fetchAll() {
	return fetch("");
    }

    public T fetchById(Object id) {
	return fetchFirst(" id = " + id);
    }

    public List<T> fetch(String where) {
        try {
            int i = 0;
            while (hostsBusy() && i++ < MAX_RETRIES) 
                Thread.sleep(5000);
        } catch (InterruptedException e) { 
            log.error("Error pausing thread", e); 
        }
	List<T> rows = new ArrayList();
	String qry = "SELECT * FROM " + type.table() + 
	    (null==where || where.equals("") || Pattern.matches(
                "(?)\\s*(limit|where).*", where)? "": " WHERE ") + where;
	log.info("fetch.qry = " + qry);
	for (Row row : session.execute(qry))
	    rows.add((T) toDatum(row, type));
	log.info("fetch.rows = " + rows.size());
	return rows;
    }

    public boolean hostsBusy() {
        for (Host host : session.getState().getConnectedHosts()) {
            log.info("fetch.host = " + host + ", inflightQueries = " + 
                session.getState().getInFlightQueries(host));
            if (session.getState().getInFlightQueries(host)< maxInflightQueries)
                return false;
        }
        return true;
    }

    public static Datum toDatum(Row row, Datum type) {
	Datum d = type.newInstance();
	Map<String, Object> cols = type.colMap();
	log.trace("toDatum.columns = " + type.columns());
	for (String col : type.columns()) {
	    if (cols.get(col) instanceof Integer)
	        d.put(col, row.getInt(col));
	    else if (cols.get(col) instanceof Set)
	        d.put(col, Datum.fromDb(row.getSet(col, String.class)));
	    else if ("subtimes".equals(col)) 
	        d.put(col, new ArrayList(row.getList(col, Date.class)));
	    else if (cols.get(col) instanceof List) 
	        d.put(col, Datum.fromDb(row.getList(col, String.class)));
	    else if ("id".equals(col) || cols.get(col) instanceof UUID)
	        d.put(col, row.getUUID(col));
            else if (cols.get(col) instanceof Map)
                if (!((Map)cols.get(col)).isEmpty() &&
	             ((Map)cols.get(col)).values().iterator().next() 
                    instanceof List) 
                    d.put(col, Datum.fromDb(
                        row.getMap(col, TypeToken.of(String.class),
                        new TypeToken<List<String>>() {})));
                else
                    d.put(col, Datum.fromDb(row.getMap(col, String.class,
                        String.class)));
	    else 
	        d.put(col, Datum.fromDb(row.getString(col)));
	}
	return d;
    }

    public T fetchFirst(String whereClause) {
	List<T> rows = fetch(whereClause + " limit 1");
	return rows.size() == 0? null: rows.get(0);
    }

    public static String contains(String field, List<String> options, 
	String andOr) {
	StringBuilder q = new StringBuilder();
	for (String opt : options)
	    q.append(q.length()==0? "": andOr).append(field).
	        append(" CONTAINS ").append(opt);
	return q.toString();
    }

    public static String in(List<String> options) {
	StringBuilder q = new StringBuilder(" IN ( ");
	for (String opt : options)
	    q.append("'").append(opt).append("'");
	return q.append(" ) ").toString();
    }

    public void close() {
	session.close();
	cluster.close();
	log.trace("db.testClient done");
    }

}
