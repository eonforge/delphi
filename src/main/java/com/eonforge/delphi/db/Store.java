package com.eonforge.delphi.db;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class Store {


    public Store() { 
    }

    public void persist(Datum datum) {
	// solr doc (node): text, nodeid, perfscore, templateid, inputs, output
	//                  dbid
	// solr doc (template): description, name, modules, dbid
	// solr doc (resource): type (FS, db, search, stream), URI, description
	// cassandra node: parentid, id, text, before, after, iscmmt, childids
	//                 perfscore, template, inputs, outputs, statediffs,
	//                 package
	// cassandra template: modules, packages, api, usecases, description
        updateSolr(datum.table(), 
			toXml(Arrays.asList("add" ,"doc"), datum.colMap())); 
	updateDb(toInsertSql(datum.table(), datum.colMap()));
    }

    public List<Datum> retrieve(String query) {
        return Collections.emptyList();
    }

    private void updateSolr(String collection, String xml) {
	
    }

    private void updateDb(String sql) {
    }


    private String toXml(List<String> elements, Map<String, Object> fields) {
	StringBuilder xml = new StringBuilder();
	for (String element : elements) 
	    xml.append("<").append(element).append(">");
	for (String field : fields.keySet())
	    xml.append("<field name='").append(field).append("'>")
	        .append(fields.get(field)+"").append("<\\/field>");
	for (String element : elements)
	    xml.append("<\\/").append(element).append(">");
	return xml.toString();
    }

    private String toSelectSql(String table, Map<String, Object> fields) {
	StringBuilder sql = new StringBuilder("SELECT * FROM").
  	    append(table).append(" WHERE id = ").append(fields.get("id"));
	return sql.toString();
    }

    private String toInsertSql(String table, Map<String, Object> fields) {
	StringBuilder sql = new StringBuilder("INSERT INTO ").
		append(table).append(" (");
	int i = fields.keySet().size();
	for (String field : fields.keySet())
	    sql.append(field).append(--i > 0? ", ": "");
	sql.append(") values (");
	for (String field : fields.keySet())
	    sql.append(fields.get(field)+"").append(--i > 0? ", ": ")");
	return sql.toString();
    }

    private String toUpdateSql(String table, Map<String, String> fields) {
	StringBuilder sql = new StringBuilder("UPDATE ").
		append(table).append(" SET ");
	int i = fields.keySet().size();
	for (String field : fields.keySet())
	    sql.append(field).append("=").append(fields.get(field))
	        .append(--i > 0? ", ": "");
	sql.append(" WHERE id = ").append(fields.get("id"));
	return sql.toString();
    }


}
