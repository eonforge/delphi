package com.eonforge.delphi.db;

import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import com.eonforge.peregrine.parse.Json;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Datum extends HashMap<String, Object> {
  
    public abstract String table();
    public abstract Datum newInstance(Map<String, Object>... data);
    public abstract Map<String, Object> colMap();
    private static final Logger log = LoggerFactory.getLogger(Datum.class);

    private Map<String, Object> map() { 
	// Guarantee to grab data added without properly using the put() method
	for (Entry<String, Object> ent : colMap().entrySet()) { 
	    put(ent.getKey(), ent.getValue()); 
	}
	return this;
    }

    public Set<String> columns() { return map().keySet(); }


    public Object get(String col) { return super.get(col); }

    public Object get(String col, Object def) { 
	return super.get(col)==null? def : super.get(col); 
    }

    public Object putBasic(String k, Object v) {
	return super.put(k, v);
    }

    public static UUID toUUID(String s) {
        if (s == null || s.isEmpty()) return UUID.randomUUID();
        String str = "";
        for (int i = s.length()-1; i >= 0; i--) str += s.charAt(i);
        for (int i = str.length(); i < 16; i++) str += " ";
        byte[] encoded = Base64.getEncoder().encode(str.getBytes());
        UUID uuid = UUID.nameUUIDFromBytes(encoded);
        return uuid;
    }

    public String valStr(String column) {
	Map<String, Object> m = map();
	if (m.get(column) instanceof Integer)
	    return String.valueOf(m.get(column));
	else if (m.get(column) instanceof Set) 
	    return "{" + ((Set) m.get(column)).stream().map(Datum::quote).
	        collect(Collectors.joining(", ")) + "}";
	else if (m.get(column) instanceof List) 
	    return "["+((List) m.get(column)).stream().map(Datum::quote).
	        collect(Collectors.joining(", ")) + "]";
	else if (m.get(column) instanceof Map) 
	    return Json.toJsonString(quoteM(m.get(column))).
                replaceAll("\"","'");
	else if (m.get(column) instanceof UUID || "id".equals(column))
	    return String.valueOf(m.get(column));
	return quote(String.valueOf(m.get(column)==null ? "" : m.get(column)));
    }

    public static String quote(Object o) {
	if (o instanceof String) 
            return "'" + toDb((String) o)+ "'";
	return o + "";
    }

    public static Map quoteM(Object o) {
        Map map = (Map) o;
        for (Object key : new ArrayList(map.keySet())) {
            Object v = map.get(key);
            map.remove(key);
            key = key instanceof String ? toDb(key) : key;
            if (v instanceof String)
                map.put(key, toDb(v));
            else if (v instanceof List)
                map.put(key, ((List) v).stream().map(Datum::toDb).
                    collect(Collectors.toList()));
            else map.put(key, v);
        }
        return map;
    }

    public static String toDb(Object o) {
        String s = o == null ? "" : o.toString().replaceAll("'", "<squote>").
            replaceAll("\\{\\}","<curlies>").replaceAll("\\'", "<bquote>").
            replaceAll("\\[\\]", "<squares>").replaceAll("\"", "<dquote>").
            replaceAll("\\?", "<qn>").replaceAll(",", "<comma>").
            replaceAll(":", "<colon>").replace(""+'"', "<quote>");
        log.trace("toDb().s = " + s);
        return s;
    }

    public static Object fromDb(Object o) {
        if (o instanceof String) 
            return fromDb((String) o);
        if (o instanceof Map) 
            return fromDb((Map) o);
        if (o instanceof List) 
            return fromDb((List) o);
        if (o instanceof Set) 
            return fromDb((Set) o);
        return o;
    }

    public static String fromDb(String s) {
        s = s == null ? s : s.replaceAll("<curlies>", "{}").
             replaceAll("<squote>", "'").replaceAll("<squares>", "\\[\\]").
             replaceAll("<dquote>","\"").replaceAll("<bquote>","\\'").
             replaceAll("<qn>", "?").replaceAll("<comma>", ",").
             replaceAll("<colon>", ":").replaceAll("<quote>", ""+'"').
             replaceAll("\\\\\\\\\"", "\\\\\"").
             replaceAll("\\\\\\\\\'", "\\\\\'").
             replaceAll("'\\\\\\\\n'", "'\\\\n'");
        log.trace("fromDb().s = " + s);
        return s;
    }

    public static Map fromDb(Map m) {
        if (m == null) return null;
        for (Object k : new ArrayList(m.keySet())) {
            Object dbK = fromDb(k);
            m.put(dbK, fromDb(m.get(k)));
            if (!k.equals(dbK)) m.remove(k);
        }
        log.trace("fromDb().m = " + m);
        return m;
    }

    public static Set fromDb(Set s) {
        if (s == null) return null;
        Set set = new HashSet();
        for (Object o : s) set.add(fromDb(o));
        return set;
        //return s.stream().map(Datum::fromDb).collect(Collectors.toSet());
    }

    public static List<String> fromDb(List<String> list) {
        if (list == null) return null;
        return list.stream().map(Datum::fromDb).collect(Collectors.toList()); 
    }

    public static String escape(String s, String tok) {
	return s.indexOf(tok) < 0? s : s.replaceAll(tok, "\\\\" + tok);
    }

    public boolean contains(String k) { return map().containsKey(k); }

    public static Map toMap(Object ... kvs) {
	Map map = new HashMap();
	for (int i = 0; i+1 < kvs.length; i+=2) 
	    map.put(kvs[i], kvs[i+1]);
	return map;
    }

    public static <T,U> Map<T,U> dissoc(Map<T,U> map, T... keys) {
        if (map == null) return null;
        for (T k : keys) map.remove(k);
        log.trace("dissoc() map = " + map + ", keys = " + Arrays.asList(keys));
        return map;
    }

    public <T> Datum merge(T ... kvs) {
	for (int i = 0; i+1 < kvs.length; i+=2) 
	    put(String.valueOf((Object)kvs[i]), kvs[i+1]);
	return this;
    }

    public <T,U> Datum merge(Map<T,U> b, T... keys) {
        log.trace("merge.b = " + b);
	if (b==null) return this;
	for (T k : keys) put(String.valueOf((Object)k), b.get(k));
        log.trace("merge.keys = " + b.keySet());
	if (keys.length==0) 
	    for (T k : b.keySet()) 
		put(String.valueOf((Object)k), b.get(k));
	return this;
    }

    public <T,U> Datum mergeAll(Map<T,U> b) {
        return merge(b, b.keySet().toArray());
    }

    public Datum merge(Map<String, Object> other, String... keys) {
        Map<String, Object>  b = other;
        log.trace("merge.b = " + b + ", keys = " + Arrays.asList(keys));
	if (b==null) return this;
	for (String k : keys) put(k, b.get(k));
        log.trace("merge.b.keys = " + b.keySet());
	if (keys.length==0)
            for (String k : b.keySet()) 
                if (!Arrays.asList("properties", "listproperties").contains(k)) 
                    put(k, b.get(k));
	        else put(k, Datum.merge((Map<String,Object>) get(k), 
 				        (Map<String,Object>) b.get(k)));
        log.trace("merge.result = " + this);
	return this;
    }

    public String getKey(String prefix, String regex) {
        int i = 0;
        Object v = get(prefix + i++);
        while (v != null) {
            if (v != null && Pattern.matches(regex, v.toString()))
                return prefix + (i-1);
            v = get(prefix + i++);
        }
        return null;
    }

    public List<String> getKeys(String prefix) {
        List<String> keys = new ArrayList();
        for (String k : keySet()) 
            if (k.startsWith(prefix)) keys.add(k);
        if (get("properties") != null)
            for (String k : ((Map<String, Object>) get("properties")).keySet())
                if (k.startsWith(prefix)) keys.add(k);
        if (get("listproperties") != null)
            for (Object k : ((Map) get("listproperties")).keySet())
                if (k.toString().startsWith(prefix)) keys.add(k.toString());
        return keys;
    }
 
    public Object getValue(String prefix, String regex) {
        return get(getKey(prefix, regex));
    }
 
    public List<Object> getValues(String prefix) {
        List<Object> vals = new ArrayList();
        for (String k : getKeys(prefix)) vals.add(get(k));
        return vals;
    }

    public static List<String> lowerCase(List vals) {
        List<String> res = new ArrayList();
        for (Object v : vals) 
            res.add(v==null ? "" : v.toString().toLowerCase());
        return res;
    }

    public static <T> List<T> concat(Collection<T>... lists) {
        List<T> res = new ArrayList();
        for (Collection<T> c : lists) res.addAll(c);
        return res;
    }

    public static <T> List<T> concUnique(Collection<T>... lists) {
        List<T> res = new ArrayList();
        HashSet<T> seen = new HashSet();
        for (Collection<T> c : lists) {
            for (T o : c) {
                if (!seen.contains(o)) {
                    res.add(o);
                    seen.add(o);
                }
            }
        }
        return res;
    }

    public static <T,U> Map<T,U> merge(Map<T,U> a, Map<T,U> b) {
	if (a==null || b==null) return a==null? b: a;
        Map<T,U> merged = new HashMap<T,U>();
	for (T key : a.keySet()) merged.put(key, a.get(key));
	for (T key : b.keySet()) merged.put(key, b.get(key));
	return merged;
    }

    public static <T, U> Map<T,U> rename(Map<T,U> map, T... keyPairs) {
	for (int i = 0; i < keyPairs.length-1; i += 2) {
	    T key = keyPairs[i], newKey = keyPairs[i+1];
            U v = map.get(key);
	    map.put(key, null);
	    map.remove(key);
	    map.put(newKey, v);
	}
	return map;
    }

    public static <T, U> Map<T,U> copy(Map<T,U> map, T... keyPairs) {
	for (int i = 0; i < keyPairs.length-1; i += 2) {
	    T key = keyPairs[i], newKey = keyPairs[i+1];
            U v = map.get(key);
	    map.put(newKey, v);
	}
	return map;
    }

    public static <T, U> Map<T,U> selectKeys(Map<T,U> map, T... keys) {
        Map<T, U> newMap = new HashMap();
	for (T key : keys) 
            newMap.put(key, map.get(key));
	return newMap;
    }

    public static <T, U> Map<T,U> apply(Map<T,U> map, Function<U,U> fn, 
		    T... keys) {
        for (T key : keys) 
	    map.put(key, fn.apply(map.get(key)));
	return map;
    }

    public static <T> List<T> repeat(int size, T obj) {
        List<T> list = new ArrayList();
	for (int i = 0; i < size; i++) list.add(obj);
	return list;
    }

    public static Map toNestedMap(Object strObj) {
	String str = URLDecoder.decode(strObj==null? "": strObj.toString());
	Map<String, Object> map = new HashMap();
	for (String atok : str.split("&")) {
	    String[] toks = atok.split("=");
	    System.out.println("toks = " + Arrays.asList(toks) + ", str="+str);
	    int i = 0;
	    for (i = 0; i<toks.length; i++) 
		if (hasParent(map, toks, i)) break;
	    if (i<toks.length) continue;
	    if (toks.length <= 2) 
		map.put(toks[0], toks.length>1? toks[1]: null);
	    else {
		i = toks[0].length()==0? 1: 0;
	        Map next = toNestedMap(atok.substring(toks[i].length()+1));
		if (map.containsKey(toks[i])) 
		    ((Map)map.get(toks[i])).putAll(next);
		else map.put(toks[i], next);
	    }
	}
	return map;
    }

    public static boolean hasParent(Map<String, Object> map, String[] s, int i)
    {
		String parent = "", tok = s[i];
		for (String sub : tok.split("\\.")) {
		    parent += ("".equals(parent)? "": ".") + sub;
		    if (map.containsKey(parent)) {
			String all = "";
			for (int j=i+1; j<s.length; j++) 
			    all += ("".equals(all)? "": "=") + s[j]; 
			((Map)map.get(parent)).put(tok, all.contains("=")? 
			    toNestedMap(all): all);
			return true;
		    }
		}
		return false;
    }

    public static String tail(String s) {
	return s.substring(s.lastIndexOf(".")+1);
    }

    public static Map<String, Object> tailKeys(Map<String, Object> map) {
	List<String> removeKeys = new ArrayList();
	Map<String, Object> toAdd = new HashMap();
	for (String k : map.keySet()) {
	    if (k.contains(".")) {
		toAdd.put(k.substring(k.lastIndexOf(".")+1), map.get(k));
                removeKeys.add(k);
	    }
	    if (map.get(k) instanceof Map) tailKeys((Map)map.get(k));
	}
	for (String k : removeKeys) map.remove(k);
	map.putAll(toAdd);
	return map;
    }

    // Convert maps with integer keys to lists
    public static Object renderLists(Map<String, Object> map) {
	UUID id = UUID.randomUUID();
	List vals = repeat(map.size(), id);
	List<String> remove = new ArrayList();
	boolean isListMap = false;
	for (String k : map.keySet()) {
	    if (map.get(k) instanceof Map) 
		map.put(k, renderLists((Map)map.get(k)));
            if (Pattern.matches("^[0-9]+$", k)) {
		vals.set(Integer.valueOf(k), map.get(k));
		remove.add(k);
	    }
	    if (map.get(k) instanceof List) isListMap = true;
	    if (map.get(k) == null || "".equals(map.get(k))) remove.add(k);
	}
	vals.removeAll(Arrays.asList(id));
	if (!vals.isEmpty()) {
	    if (map.keySet().size() == remove.size()) return vals;
	    else for (String k : remove) map.remove(k);
	}
	if (isListMap) for (String k : remove) map.remove(k);
	return map;
    }

    public static Map<String, Object> decode(Map<String, String> params) {
	//System.out.println(renderLists(tailKeys(toNestedMap(url))));
	String url = "";
	for (String k : params.keySet())
	    url += ("".equals(url)? "": "&") + k + "=" + params.get(k);
	return (Map) renderLists(tailKeys(toNestedMap(url)));
    }

    public static List unique(List data, Function keyFn) {
        Map entries = new HashMap();
        List values = new ArrayList();
        for (Object d : data) {
            Object key = keyFn.apply(d);
            if (!entries.containsKey(key)) {
                entries.put(key, true);
                values.add(d);
            }
        }
        return values;
    }

    public static boolean isEmpty(Object obj) {
	return obj == null || obj.toString().trim().length() == 0;
    }
}
