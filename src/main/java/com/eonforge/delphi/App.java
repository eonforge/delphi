package com.eonforge.delphi;

import com.eonforge.delphi.graph.GraphUtil;
import com.eonforge.delphi.db.Client;
import java.util.Arrays;

/**
 *  App launcher
 */
public class App 
{
    public static void main( String[] args )
    {
	if (args.length == 0) 
	    System.out.println("Usage: App [-g yaml|-h host -ks keyspace -p port]");
	String host = null, keyspace = null, port = null;
	for (int i = 0; i < args.length-1; i++) {
	    if (args[i].equals("-g"))
	        GraphUtil.testClient(args[i+1]);
	    else if (args[i].equals("-h"))
		host = args[i+1];
	    else if (args[i].equals("-ks"))
		keyspace = args[i+1];
	    else if (args[i].equals("-p"))
		port = args[i+1];
	}
	if (host != null && keyspace != null)
	    Client.instance(host, port, keyspace).test("node").close();
        System.out.println( "Done" );
	int cont = 1;
	while (cont == 1) { };
    }
}
