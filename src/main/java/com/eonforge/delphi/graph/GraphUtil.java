package com.eonforge.delphi.graph;

import java.util.stream.Collectors;
import org.apache.tinkerpop.gremlin.structure.*;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerFactory;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.ResultSet;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;

public class GraphUtil {

    public GraphUtil() {}

    public static Graph testGraph() {
        Graph graph = TinkerGraph.open(); 
        Vertex marko = graph.addVertex(T.label, "person", T.id, 1, "name", "marko", "age", 29); 
        Vertex vadas = graph.addVertex(T.label, "person", T.id, 2, "name", "vadas", "age", 27);
        Vertex lop = graph.addVertex(T.label, "software", T.id, 3, "name", "lop", "lang", "java");
        Vertex josh = graph.addVertex(T.label, "person", T.id, 4, "name", "josh", "age", 32);
        Vertex ripple = graph.addVertex(T.label, "software", T.id, 5, "name", "ripple", "lang", "java");
        Vertex peter = graph.addVertex(T.label, "person", T.id, 6, "name", "peter", "age", 35);
        marko.addEdge("knows", vadas, T.id, 7, "weight", 0.5f); 
        marko.addEdge("knows", josh, T.id, 8, "weight", 1.0f);
        marko.addEdge("created", lop, T.id, 9, "weight", 0.4f);
        josh.addEdge("created", ripple, T.id, 10, "weight", 1.0f);
        josh.addEdge("created", lop, T.id, 11, "weight", 0.4f);
        peter.addEdge("created", lop, T.id, 12, "weight", 0.2f);
	System.out.println("MD: graph = " + graph);
	return graph;
    }

    public static void testClient(String yaml) {
	GraphTraversalSource g = testGraph().traversal();
	try {
	    Cluster cluster = yaml == null? Cluster.open(): Cluster.open(yaml);
	    Client client = cluster.connect();
	    Client aliased = client.alias("g");
	    ResultSet results = aliased.submit(g.V());
	    results.stream().map(i -> { 
		    System.out.println("result = " + i); return i;
	    } );
	    System.out.println("testClient.results = " + results.stream().
	        collect(Collectors.toList()));
	} catch (Exception e) {
	    System.err.println(e.getMessage());
	    e.printStackTrace();
	}
    }
}
