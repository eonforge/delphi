package com.eonforge.delphi.db;

import java.util.*;
import java.util.stream.Collectors;
import org.junit.Test;
import org.junit.Assert;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.datastax.driver.core.Row;

import com.eonforge.peregrine.Config;
import com.eonforge.delphi.db.model.Node;

/**
 * Unit test for db client
 */
public class ClientTest 
{

    private static final Logger log = LoggerFactory.getLogger(ClientTest.class);

    public ClientTest( )
    {
	Config.load("delphi.properties");
    }

    @Test
    public void testBasicDatum() {
	Datum type = new Node(); //newDatum();
	Client<Node> db = Client.instance(new Node());
	String name = "ab'c{}de\"f\\\'xyz";
        type.put("name", name);
        type.put("{COMPANY(\"Company\"):", "=COMPANY(\"Company\");");
	db.insert(type);
	log.info("testBasicDatum.type = " + type);
	int count = 0;
	for (Node row : db.fetch("name = 'ab<squote>c<curlies>de<dquote>f" +
            "\\<squote>xyz'")) {
	    count++;
	    log.info("row = " + row);
	    Assert.assertNotNull(row);
	    Assert.assertEquals(row.get("name"), name);
	}
	Assert.assertTrue(count > 0);
	db.delete(type);
	log.info("test done");
    }

    @Test
    public void testMockDb() {
	Client db = Client.instance(new Node()); // Will initialize mock db
	int count = 0;
	Datum ent = null;
	for (Object row : db.fetchAll()) { // fetches mock db records
	    count++;
	    ent = (Datum) row;
	    log.info("row = " + row);
	}
        Assert.assertTrue(count >= 2);
	if (ent != null) db.insert(ent);
	log.info("test done");
    }

    private static Datum newDatum() {
	return new Datum() {
	    public String table() { return "kbase.node"; }
	    public Map<String, Object> colMap() {
		Map<String, Object> m = new HashMap(this);
		m.put("name", "test'name");
		m.put("properties", Arrays.asList("prop1").stream().collect(
		    Collectors.toSet()));
		m.put("operations", Arrays.asList("op1").stream().collect(
		    Collectors.toSet()));
		m.put("edges", Arrays.asList("edge1").stream().collect(
		    Collectors.toSet()));
		return m;
	    }
            public Datum newInstance(Map<String, Object>... m) { return this; }
	};
    }
}
