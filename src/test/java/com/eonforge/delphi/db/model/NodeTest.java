package com.eonforge.delphi.db;

import java.util.*;
import java.util.stream.Collectors;
import org.junit.Test;
import org.junit.Assert;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eonforge.peregrine.Config;
import com.eonforge.delphi.db.model.Node;

/**
 * Unit test for Node class.
 */
public class NodeTest 
{

    private static final Logger log = LoggerFactory.getLogger(NodeTest.class);

    public NodeTest( )
    {
	Config.load("delphi.properties");
    }

    @Test
    public void testParseMethodDef() {
	Node n = Node.parseDef("public int getId(String pname, int size) {");
	Assert.assertEquals("getId", n.name);
	Assert.assertEquals("int", n.retval);
	Assert.assertEquals(1, n.qualifiers.size());
	Assert.assertEquals("public", n.qualifiers.get(0));
	Assert.assertTrue(n.nodes.size() == 2); 
	log.info("testParseMethodDef.params = " + n.nodes);
	Assert.assertEquals("pname", n.nodes.get(0).get("name"));
	Assert.assertEquals("String", n.nodes.get(0).retval);
	n = Node.parseDef("public int getId() {");
	log.info("testParseMethodDef.n = " + n);
	Assert.assertEquals("getId", n.name);
	Assert.assertEquals("int", n.retval);
	Assert.assertEquals(1, n.qualifiers.size());
	Assert.assertEquals("public", n.qualifiers.get(0));
	Assert.assertTrue(n.nodes.isEmpty()); 
	n = Node.parseDef(" void getId();");
	Assert.assertEquals("getId", n.name);
	Assert.assertEquals("void", n.retval);
	log.info("testParseMethodDef done");
	n = Node.parseDef("GetId(){");
	Assert.assertEquals("GetId", n.name);
	Assert.assertEquals(null, n.retval);
	log.info("testParseCtorDef done");
        String def = "private static final byte STATUS_REQRES = 1 << 1;\n " +
            "final byte STATUS_REQRES = 1 << 2; \n" +
            "private static final byte STATUS_HANDSHAKE = 1 << 3; \n" +
            "public static boolean isRequest(byte value);";
	String[] toks = Node.splitMethod(def)[0].split("[\n\\s]");
	Assert.assertEquals("isRequest", toks[toks.length-1]);
	Assert.assertEquals("boolean", toks[toks.length-2]);
	log.info("testParseClassMethodDef done");
    }

    @Test
    public void testParseCtor() {
	Node n = Node.parseDef("public SomeClass(String content) {");
	Assert.assertEquals("SomeClass", n.name);
	Assert.assertEquals(0, n.qualifiers.size());
	Assert.assertEquals("public", n.retval);
	log.info("testParseCtor done");
    }

    @Test
    public void testName() {
	Node n = Node.parseDef("public SomeClass(String content) {");
	Assert.assertEquals("SomeClass", n.name);
	Assert.assertEquals("SomeClass", (String)n.colMap().get("name"));
	log.info("keys = " + Arrays.asList(n.colMap().keySet().toArray()));
	log.info("testName done");
    }
    
    @Test
    public void testSplitMethod() {
        String meth = "  @Action(domainEvent = UpdateNameDomainEvent.class)\n"+
           "  public SimpleObject updateName(\n" +
           "@Parameter(maxLength = 40) @ParameterLayout(named = \"New name\")"+
           " final String name) {";
        List<String> splits = Node.splitTopLevel(meth, '(', ')', null);
        log.info("testSplitMethod.meth = " + meth);
        log.info("testSplitMethod.splits = " + splits);
        String classDef = splits.get(0) +"("+ splits.get(1) +")"+ splits.get(2);
        log.info("testSplitMethod.classDef = " + classDef);
        Assert.assertEquals("@Action", splits.get(0).trim());
        Assert.assertEquals("domainEvent = UpdateNameDomainEvent.class", 
            splits.get(1));
        Assert.assertEquals("@Parameter(maxLength = 40) @ParameterLayout(" +
            "named = \"New name\") final String name", splits.get(3).trim());
        Assert.assertEquals("@Action(domainEvent = UpdateNameDomainEvent.class)",
            Node.getClassAnnotations(classDef).get(0));
    }
}
