# Delphi - Database layer

[Delphi](https://en.wikipedia.org/wiki/Delphi) is famous as the ancient sanctuary that the oracle consulted about important decisions throughout the ancient classical world. Moreover, the Greeks considered Delphi the navel (or centre) of the world, as represented by the stone monument known as the Omphalos of Delphi.


## Compiling

- This is a maven project, so use maven commands, e.g. 

```
mvn clean package
```

## Updating maven repository

For development purposes, updating snaphots is necessary so changes are picked up by dependent projects. 
```
mvn clean install -U
or
mvn dependency:purge-local-repository clean package
```
